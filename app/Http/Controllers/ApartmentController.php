<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use App\Models\Resident;
use App\Models\Shahrak;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use function PHPUnit\Framework\returnSelf;


class ApartmentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('apartment_show'))
          return view('user.access');

        $data = Apartment::all();
        return view('apartment.listapts',['apartment' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('apartment_add'))
          return view('user.access');

        $shahraks = Shahrak::orderBy('shahrak_name')->pluck('shahrak_name','id');
        $residents = Resident::orderBy('name')->get();
        return view('apartment.addApartment', compact('shahraks','residents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->can('apartment_add'))
          return view('user.access');

         $request->validate([
            "resident_id" => "required",
            "shahrak_id" => "required",
            "tip" => "required",
            "type" => "required",
            "room" => "required",
            "hall" => "required",
            "kitchen" => "required",
            "bathroom" => "required",
            "floor" => "required",
            "block" => "required",
            "Areasize" => "required",
            "Totalprice" => "required",
            "percentage" => "required",
            "NetAmount" => "required",
            "date" => "required",
            'ApartmentNo' => 'required|unique:apartments,apartment_no,'.$request->ApartmentNo.',id,shahrak_id,'.$request->shahrak_id,


          ]);

          $Jdate = explode('/', faTOen($request->date));
          $j_y = $Jdate[0];
          $j_m = $Jdate[1];
          $j_d = $Jdate[2];

          $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

            $Apartment = new Apartment;
            $Apartment->apartment_no = $request->ApartmentNo;
            $Apartment->shahrak_id = $request->shahrak_id;
            $Apartment->resident_id = $request->resident_id;
            $Apartment->hall_no = $request->hall;
            $Apartment->tip = $request->tip;
            $Apartment->block = $request->block;
            $Apartment->type = $request->type;
            $Apartment->net_amount = $request->NetAmount;
            $Apartment->rooms = $request->room;
            $Apartment->bathrooms = $request->bathroom;
            $Apartment->kitchen = $request->kitchen;
            $Apartment->floorNo = $request->floor;
            $Apartment->cost = $request->Totalprice;
            $Apartment->percentage = $request->percentage;
            $Apartment->net_amount = $request->NetAmount;
            $Apartment->area_size = $request->Areasize;
            $Apartment->east = $request->East;
            $Apartment->west = $request->West;
            $Apartment->north = $request->North;
            $Apartment->south = $request->South;
            $Apartment->date = $Gdate;

            $Apartment->save();

         return redirect()->route('apartment.index')->with('message', ' apaeretmeent number  '.$request->ApartmentNo.'adeded succssfully');




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function show(apartment $apartment)
    {
        if(!Auth::user()->can('apartment_show'))
          return view('user.access');

        return view('apartment.show',compact('apartment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function edit(apartment $apartment)
    {
        if(!Auth::user()->can('apartment_edit'))
          return view('user.access');

        $shahraks = Shahrak::orderBy('shahrak_name')->pluck('shahrak_name','id');
        $residents = Resident::orderBy('name')->get();
        return view('apartment.edit', compact('apartment','shahraks','residents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, apartment $apartment)
    {
        if(!Auth::user()->can('apartment_edit'))
          return view('user.access');

        $request->validate([
          "resident_id" => "required",
          "shahrak_id" => "required",
          "tip" => "required",
          "type" => "required",
          "room" => "required",
          "hall" => "required",
          "kitchen" => "required",
          "bathroom" => "required",
          "floor" => "required",
          "block" => "required",
          "Areasize" => "required",
          "Totalprice" => "required",
          "percentage" => "required",
          "NetAmount" => "required",
          "date" => "required",
          // 'ApartmentNo' => 'required|unique:apartments,apartment_no,'.$request->ApartmentNo.',id,shahrak_id,'.$request->shahrak_id,

          ]);

          $Jdate = explode('/', faTOen($request->date));
          $j_y = $Jdate[0];
          $j_m = $Jdate[1];
          $j_d = $Jdate[2];

          $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

           $Apartment = Apartment::find($apartment->id);
           // $Apartment->apartment_no = $request->ApartmentNo;
           $Apartment->shahrak_id = $request->shahrak_id;
           $Apartment->resident_id = $request->resident_id;
           $Apartment->hall_no = $request->hall;
           $Apartment->block = $request->block;
           $Apartment->type = $request->type;
           $Apartment->rooms = $request->room;
           $Apartment->bathrooms = $request->bathroom;
           $Apartment->kitchen = $request->kitchen;
           $Apartment->floorNo = $request->floor;
           $Apartment->cost = $request->Totalprice;
           $Apartment->percentage = $request->percentage;
           $Apartment->area_size = $request->Areasize;
           $Apartment->east = $request->East;
           $Apartment->west = $request->West;
           $Apartment->north = $request->North;
           $Apartment->south = $request->South;
           $Apartment->date = $Gdate;



        $Apartment->save();
        return redirect()->route('apartment.index')->with('message', 'land number '.$request->ApartmentNo.'Updated succssfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(apartment $apartment)
    {
        if(!Auth::user()->can('apartment_delete'))
          return view('user.access');

        $apartment = apartment::find($apartment->id);
        $apartment->delete();

        return redirect()->route('apartment.index')->with('message', __('general.record_deleted'));


    }
}
