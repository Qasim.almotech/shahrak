<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use App\Models\Land;
use App\Models\Apartment;
use App\Models\Shahrak;
use Auth;
use League\CommonMark\Node\Block\Document as BlockDocument;

class DocumentController extends Controller
{
      /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct()
      {
          // $this->middleware('guest');
          $this->middleware('auth');
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('document_show'))
          return view('user.access');

        $doc = Document::all();
        return view('document.index',['doc' => $doc]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('document_add'))
          return view('user.access');
        $apartments = Apartment::all();
        $lands = Land::all();
        $shahrak = Shahrak::all();

        return view('document.create', compact('lands', 'apartments', 'shahrak'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(!Auth::user()->can('document_add'))
          return view('user.access');

        $request->validate([
            "type" => "required",
            "apartment_id" => "required_without:land_id",
            "land_id" => "required_without:apartment_id",
            "date" => "required",
            'file' => 'required|max:2048',

        ]);
      //   $validator = Validator::make($request->all(), [
      //     'file' => 'max:500',
      // ]);

        $Jdate = explode('/', faTOen($request->date));
        $j_y = $Jdate[0];
        $j_m = $Jdate[1];
        $j_d = $Jdate[2];
        $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

        $document = new Document;

        if($request->type == 'land')
          $document->land_id = $request->land_id;

        if($request->type == 'apartment')
          $document->apartment_id = $request->apartment_id;

        if($request->type == 'shahrak')
          $document->shahrak_id = $request->shahrak_id;

        if($request->hasFile('file')) {
            $ext = $request->file('file')->getClientOriginalExtension();
            $file = $request->land_id.'-'.$request->type_id.'.'.$ext;
            $request->file('file')->storeAs('public/images/',$file );
        }

        if($request->type == 'apartment')
        $document->apartment_id = $request->apartment_id;
        if($request->hasFile('file')) {
            $ext = $request->file('file')->getClientOriginalExtension();
            $file = $request->apartment_id.'-'.$request->type_id.'.'.$ext;
            $request->file('file')->storeAs('public/images/',$file );
        }

        if($request->type == 'shahrak')
            $document->shahrak_id = $request->shahrak_id;

        if($request->hasFile('file')) {
            $ext = $request->file('file')->getClientOriginalExtension();
            $file = $request->shahrak_id.'-'.$request->type_id.'.'.$ext;
            $request->file('file')->storeAs('public/images/',$file );
        }



        $document->extradetails = $request->details;
        $document->type = $request->type_id;
        $document->path = $file;
        $document->date = $Gdate;
        $document->save();

      return redirect()->route('document.index')->with('message',  );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(document $document)
    {

        if(!Auth::user()->can('document_show'))
          return view('user.access');
       return '<img src="../storage/images/'.$document->path.'" >';

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(document $document)
    {
        if(!Auth::user()->can('document_delete'))
          return view('user.access');

        $document = document::find($document->id);
        $document->delete();

        return redirect()->route('document.index')->with('message', __('general.record_deleted'));

    }
}
