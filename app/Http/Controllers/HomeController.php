<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\Shahrak;
use App\Models\Resident;
use App\Models\Land;
use App\Models\Apartment;
use Lang;

use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
     {
        $shahrak = Shahrak::all();
        $resident = Resident::all();
        $lands = Land::all();
        $apartments = Apartment::all();
        return view('dashboard.index', compact('shahrak', 'resident', 'lands', 'apartments'));

    }
    public function countapartment()
    {
         $apartment = DB::table('shahraks')
         ->selectRaw('shahrak_name, count(apartments.id) as ApartmentID')
         ->join('apartments', 'shahraks.id', '=', 'apartments.shahrak_id')
         ->groupBy('shahrak_name')
         ->get();
          return response()->json($apartment);
    }
    public function countland()
    {
        $land = DB::table('shahraks')
        ->selectRaw('shahrak_name, count(lands.id) as landID')
        ->join('lands', 'shahraks.id', '=', 'lands.shahrak_id')
        ->groupBy('shahrak_name')
        ->get();

        return response()->json($land);
    }
    public function privateandgovernment()
    {
          $apartment=DB::table('apartments')
          ->selectRaw('apartments.type as apartmentstype, count(apartments.type) as apartmentsCount')
          ->groupBy('apartments.type')->get();

         $land=DB::table('lands')
         ->selectRaw('lands.type as LandType, count(lands.type) as landcount')
         ->groupBy('lands.type')->get();
         $data = [];
         foreach($land as $landtype){
            $data[Lang::get('general.land_'.$landtype->LandType)] = $landtype->landcount;
         }
         foreach($apartment as $apartmenttype){
            $data[Lang::get('general.apartment_'.$apartmenttype->apartmentstype)] = $apartmenttype->apartmentsCount;
         }


         return response()->json($data);
    }


}
