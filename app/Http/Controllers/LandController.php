<?php

namespace App\Http\Controllers;

use App\Models\Land;
use App\Models\Shahrak;
use App\Models\Resident;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use function GuzzleHttp\Promise\all;

class LandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Land::all();

        return view('land.listland',['lands' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $shahraks = Shahrak::orderBy('shahrak_name')->pluck('shahrak_name','id');
        $residents = Resident::orderBy('name')->get();

        return view('land.addLand', compact('shahraks','residents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
          "resident_id" => "required",
          "shahrak_id" => "required",
          "type" => "required",
          "district" => "required",
          "Areasize" => "required",
          "Totalprice" => "required",
          "percentage" => "required",
          "net_amount" => "required",
          "date" => "required",
          'landno' => 'required|unique:lands,land_no,'.$request->landno.',id,shahrak_id,'.$request->shahrak_id,

        ]);

        $Land = new Land;
         $Jdate = explode('/', faTOen($request->date));
         $j_y = $Jdate[0];
         $j_m = $Jdate[1];
         $j_d = $Jdate[2];
         $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

         $Land->resident_id = $request->resident_id;
         $Land->shahrak_id = $request->shahrak_id;
         $Land->land_no = $request->landno;
         $Land->district = $request->district;
         $Land->east = $request->East;
         $Land->west = $request->West;
         $Land->north = $request->North;
         $Land->south = $request->South;
         $Land->area_size = $request->Areasize;
         $Land->total_price = $request->Totalprice;
         $Land->percentage = $request->percentage;
         $Land->net_amount = $request->net_amount;
         $Land->date = $Gdate;
         $Land->resident_id = $request->resident_id;
         $Land->shahrak_id = $request->shahrak_id;
         $Land->type = $request->type;

      $Land->save();
      return redirect()->route('land.index')->with('message', 'land number '.$request->landno.'already exists in shahrak ');



     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Land  $land
     * @return \Illuminate\Http\Response
     */
    public function show(Land $land)
    {
        return view('land.show',compact('land'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Land  $land
     * @return \Illuminate\Http\Response
     */
    public function edit(Land $land)
    {

        $shahraks = Shahrak::orderBy('shahrak_name')->pluck('shahrak_name','id');
        $residents = Resident::orderBy('name')->get();
        return view('land.edit', compact('land','shahraks','residents'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Land  $land
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Land $Land)
    {
      $request->validate([
        "resident_id" => "required",
        "shahrak_id" => "required",
        "type" => "required",
        "district" => "required",
        "Areasize" => "required",
        "Totalprice" => "required",
        "percentage" => "required",
        "net_amount" => "required",
        "date" => "required",
        // 'landno' => 'required|unique:lands,land_no,'.$request->landno.',id,shahrak_id,'.$request->shahrak_id,

      ]);

        $Jdate = explode('/', faTOen($request->date));
        $j_y = $Jdate[0];
        $j_m = $Jdate[1];
        $j_d = $Jdate[2];
        $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

        $Land = Land::find($Land->id);
        $Land->resident_id = $request->resident_id;
        $Land->shahrak_id = $request->shahrak_id;
        $Land->land_no = $request->landno;
        $Land->district = $request->district;
        $Land->east = $request->East;
        $Land->west = $request->West;
        $Land->north = $request->North;
        $Land->south = $request->South;
        $Land->area_size = $request->Areasize;
        $Land->total_price = $request->Totalprice;
        $Land->percentage = $request->percentage;
        $Land->net_amount = $request->net_amount;
        $Land->date = $Gdate;
        $Land->resident_id = $request->resident_id;
        $Land->shahrak_id = $request->shahrak_id;
        $Land->type = $request->type;
        $Land->save();

      return redirect()->route('land.index')->with('message', 'land number '.$request->landno.'adeded succssfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Land  $land
     * @return \Illuminate\Http\Response
     */
    public function destroy(Land $land)
    {
        $land = Land::find($land->id);
        $land->delete();

      return redirect()->route('land.index')->with('message', __('general.record_deleted'));

    }
}
