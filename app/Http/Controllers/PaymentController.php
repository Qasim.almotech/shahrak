<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Models\Land;
use App\Models\Apartment;
use Auth;

class PaymentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('payment_show'))
          return view('user.access');

        $payment = Payment::all();
        return view('payment.index',['payment' => $payment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('payment_add'))
          return view('user.access');

        $apartments = Apartment::all();
        $lands = Land::all();


        return view('payment.create', compact('lands', 'apartments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->can('payment_add'))
          return view('user.access');

        $request->validate([
            "type" => "required",
            "apartment_id" => "required_without:land_id",
            "land_id" => "required_without:apartment_id",
            "awiz_no" => "required",
            "partial_no" => "required",
            "amount" => "required",
            "date" => "required",
        ]);

        $Jdate = explode('/', faTOen($request->date));
        $j_y = $Jdate[0];
        $j_m = $Jdate[1];
        $j_d = $Jdate[2];
        $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

        $payment = new Payment;
            if($request->type == 'land')
              $payment->land_id = $request->land_id;
            if($request->type == 'apartment')
              $payment->apartment_id = $request->apartment_id;

          $payment->awiz_no = $request->awiz_no;
          $payment->partial_no = $request->partial_no;
          $payment->amount = $request->amount;
          $payment->date = $Gdate;
          $payment->save();

       return redirect()->route('payment.index')->with('message', __('general.success_message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {

        return view('user.access');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        if(!Auth::user()->can('payment_edit'))
          return view('user.access');

        $apartments = Apartment::all();
        $lands = Land::all();

        return view('payment.edit', compact('payment','apartments','lands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        if(!Auth::user()->can('payment_edit'))
          return view('user.access');

        $request->validate([
            "apartment_id" => "required_without:land_id",
            "land_id" => "required_without:apartment_id",
            "awiz_no" => "required",
            "partial_no" => "required",
            "amount" => "required",
            "date" => "required",
        ]);

        $Jdate = explode('/', faTOen($request->date));
        $j_y = $Jdate[0];
        $j_m = $Jdate[1];
        $j_d = $Jdate[2];
        $Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

        if(!is_null($payment->land_id))
          $payment->land_id = $request->land_id;
        if(!is_null($payment->apartment_id))
          $payment->apartment_id = $request->apartment_id;

        $payment->awiz_no = $request->awiz_no;
        $payment->partial_no = $request->partial_no;
        $payment->amount = $request->amount;
        $payment->date = $Gdate;

        $payment->save();

        return redirect()->route('payment.index')->with('message', ''.$request->name.'Payment updated succssfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        if(!Auth::user()->can('payment_delete'))
          return view('user.access');

        $payment->delete();

        return redirect()->route('payment.index')->with('message', __('general.record_deleted'));
    }
}
