<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shahrak;
use App\Models\Land;
use App\Models\Apartment;
use Auth;

class ReportController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        // if(!Auth::user()->can(''))
        //   return view('user.access');

        $shahraks = Shahrak::all();

        return view('report.form',compact('shahraks'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
      //
      $land = Land::select('*');
      $apartment = Apartment::select('*');
      $data['land'] = [];
      $data['apartment'] = [];

          if(!empty($request->property_status))
            $land->where('type', $request->property_status);

          // if(!empty($request->pyament_status)){
          //   $land->select(DB::raw("*, SUM(payments.amount) as paid"))
          //        ->join('payments', 'land.id', '=', 'payments.land_id')
          //        ->where('land.net_amount', 'paid');
          //   // $apt->net_amount - $apt->payments->sum('amount')
          // }

          if(!is_null($request->property_status))
            $apartment->where('type', $request->property_status);

          if(!is_null($request->start_date)){

            $Jdate = explode('/', faTOen($request->start_date));
            $j_y = $Jdate[0];
            $j_m = $Jdate[1];
            $j_d = $Jdate[2];
            $start_Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

            $land->where('date', '>=', $start_Gdate);
            $apartment->where('date', '>=', $start_Gdate);
          }

          if(!is_null($request->end_date)){
            $Jdate = explode('/', faTOen($request->end_date));
            $j_y = $Jdate[0];
            $j_m = $Jdate[1];
            $j_d = $Jdate[2];
            $end_Gdate = jalali_to_gregorian($j_y, $j_m, $j_d);

            $land->where('date', '<=', $end_Gdate);
            $apartment->where('date', '<=', $end_Gdate);
          }

          if(!is_null($request->shahrak_id)){
            $land->where('shahrak_id', $request->shahrak_id);
            $apartment->where('shahrak_id', $request->shahrak_id);
          }

          if($request->property_type == 'land' || is_null($request->property_type))
            $data['land'] = $land->get();

          if($request->property_type == 'apartment' || is_null($request->property_type))
            $data['apartment'] = $apartment->get();

          return view('report.table',compact('data'));

    }
}
