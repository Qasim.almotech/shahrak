<?php

namespace App\Http\Controllers;

use App\Models\Resident;
use Illuminate\Http\Request;
use Auth;

class ResidentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('resident_show'))
          return view('user.access');

        $data = Resident::all();
        return view('resident.listresident',['residents' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $Request)
    {
      if(!Auth::user()->can('resident_add'))
        return view('user.access');

       return view('resident.addresident');
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->can('resident_add'))
          return view('user.access');

        $request->validate([
            "name" => "required",
            "f_name" => "required",
            "gf_name" => "required",
            // "page" => "required",
            // "book" => "required",
            "tazkira" => "required|min:3 ",
            "province" => "required",
            "district" => "required",
            "village" => "required"

        ]);


        $Resident = new Resident;
            $Resident->name = $request->name;
            $Resident->father_name = $request->f_name;
            $Resident->grandfather_name = $request->gf_name;
            $Resident->NIC = $request->tazkira;
            $Resident->book = $request->book;
            $Resident->page = $request->page;
            $Resident->province = $request->province;
            $Resident->district = $request->district;
            $Resident->village = $request->village;
       $Resident->save();
       return redirect()->route('resident.index')->with('message', ''.$request->name.'Resident added succssfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function show(Resident $resident)
    {
        if(!Auth::user()->can('resident_show'))
          return view('user.access');

        return view('resident.show',compact('resident'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function edit(Resident $resident)
    {
        if(!Auth::user()->can('resident_edit'))
          return view('user.access');

        return view('resident.edit', compact('resident'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resident $resident)
    {

      if(!Auth::user()->can('resident_edit'))
        return view('user.access');

        $request->validate([
            "name" => "required",
            "f_name" => "required",
            "gf_name" => "required",
            // "page" => "required",
            // "book" => "required",
            "tazkira" => "required|min:3 ",
            "province" => "required",
            "district" => "required",
            "village" => "required"

        ]);
        $resident = Resident::find($resident->id);
        $resident->name = $request->name;
        $resident->father_name = $request->f_name;
            $resident->grandfather_name = $request->gf_name;
            $resident->NIC = $request->tazkira;
            $resident->book = $request->book;
            $resident->page = $request->page;
            $resident->province = $request->province;
            $resident->district = $request->district;
            $resident->village = $request->village;
       $resident->save();
       return redirect()->route('resident.index')->with('message', ''.$request->name.'Resident updated succssfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resident $resident)
    {
        if(!Auth::user()->can('resident_delete'))
          return view('user.access');

        $resident->delete();
        return redirect()->route('resident.index')->with('message',  __('general.record_deleted'));

    }
}
