<?php

namespace App\Http\Controllers;

use App\Models\Shahrak;
use Auth;
use Illuminate\Http\Request;

class ShahrakController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('shahrak_show'))
          return view('user.access');

        $data = Shahrak::all();
        return view('shahrak.listshahrak',['Shahraks' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('shahrak_add'))
          return view('user.access');

        return view('/shahrak.addshahrak');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!Auth::user()->can('shahrak_add'))
          return view('user.access');
        $request->validate([
            "shahrakname" => "required",
            "Totalsize" => "required",
            "land_numbers" => "required",
            "Longtitude" => "required",
            "Lititude" => "required",
            "District" => "required",
            "Name" => "required",
            "GrandFather_Name" => "required",
            "Father_Name" => "required",
            "Tazkira_Number" => "required",
            "contact_number" => "required",

        ]);


        $shahrak = new Shahrak;
        $shahrak->shahrak_name = $request->shahrakname;
        $shahrak->district = $request->District;
        $shahrak->total_area_size = $request->Totalsize;
        $shahrak->no_land = $request->land_numbers;
        $shahrak->longtitude = $request->Longtitude;
        $shahrak->latitude = $request->Lititude;
        $shahrak->owner_name = $request->Name;
        $shahrak->owner_father_name = $request->Father_Name;
        $shahrak->owner_grandfather_name = $request->GrandFather_Name;
        $shahrak->tazkira_number = $request->Tazkira_Number;
        $shahrak->contact_number = $request->contact_number;
        $shahrak->extra_details = $request->Dtails;
        $shahrak->contract_company = $request->contract_company;
        $shahrak->contract_no = $request->contract_no;
        $shahrak->contract_date = $request->contract_date;
        $shahrak->sentence_no = $request->sentence_no;
        $shahrak->contract_department = $request->contract_department;
        $shahrak->monitoring = $request->monitoring;
        $shahrak->contract_percentage = $request->contract_percentage;
        $shahrak->duration_date = $request->duration_date;
        $shahrak->mutch_blocks = $request->mutch_blocks;
        $shahrak->other_area_size = $request->other_area_size;



       $shahrak->save();

       return redirect()->route('shahrak.index')->with('message',''.$request->shahrakname.' has been added  succussfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shahrak  $shahrak
     * @return \Illuminate\Http\Response
     */
    public function show(Shahrak $shahrak)
    {
      if(!Auth::user()->can('shahrak_show'))
        return view('user.access');

        $paid['land_paid'] = 0;
        foreach($shahrak->lands as $land){
          $paid['land_paid'] += $land->payments->sum('amount');
        }

        $paid['apt_paid'] = 0;
        foreach($shahrak->apartments as $apartment){
          $paid['apt_paid'] += $apartment->payments->sum('amount');
        }

        return view('shahrak.show',compact('shahrak', 'paid'));
          }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shahrak  $shahrak
     * @return \Illuminate\Http\Response
     */
    public function edit(Shahrak $shahrak)
    {
        if(!Auth::user()->can('shahrak_edit'))
          return view('user.access');

        return view('shahrak.edit', compact('shahrak'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shahrak  $shahrak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shahrak $shahrak)
    {
      if(!Auth::user()->can('shahrak_edit'))
        return view('user.access');

        $request->validate([

            "shahrakname" => "required",
            "Totalsize" => "required",
            "land_numbers" => "required",
            "Longtitude" => "required",
            "Lititude" => "required",
            "Name" => "required",
            "Father_Name" => "required",
            "Tazkira_Number" => "required|min:3 ",

        ]);

        $shahrak = Shahrak::find($shahrak->id);
            $shahrak->shahrak_name = $request->shahrakname;
            $shahrak->district = $request->District;
            $shahrak->total_area_size = $request->Totalsize;
            $shahrak->no_land = $request->land_numbers;
            $shahrak->longtitude = $request->Longtitude;
            $shahrak->latitude = $request->Lititude;
            $shahrak->owner_name = $request->Name;
            $shahrak->owner_father_name = $request->Father_Name;
            $shahrak->owner_grandfather_name = $request->GrandFather_Name;
            $shahrak->tazkira_number = $request->Tazkira_Number;
            $shahrak->contact_number = $request->contact_number;
            $shahrak->extra_details = $request->Dtails;
            $shahrak->contract_company = $request->contract_company;
            $shahrak->contract_no = $request->contract_no;
            $shahrak->contract_date = $request->contract_date;
            $shahrak->sentence_no = $request->sentence_no;
            $shahrak->contract_department = $request->contract_department;
            $shahrak->monitoring = $request->monitoring;
            $shahrak->contract_percentage = $request->contract_percentage;
            $shahrak->duration_date = $request->duration_date;
            $shahrak->mutch_blocks = $request->mutch_blocks;
            $shahrak->other_area_size = $request->other_area_size;
            $shahrak->save();
            return redirect()->route('shahrak.index')->with('message',''.$request->shahrakname.' has updated succussfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shahrak  $shahrak
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shahrak $shahrak)
    {
      if(!Auth::user()->can('shahrak_delete'))
        return view('user.access');

         $shahrak->delete();

         return redirect()->route('shahrak.index')->with('message',  __('general.record_deleted'));

    }
}
