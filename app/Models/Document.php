<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $softDelete = true;

    public function shahrak()
    {
        return $this->belongsTo(Shahrak::class, 'shahrak_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function apartment()
    {
        return $this->belongsTo(Apartment::class, 'apartment_id');
    }

}
