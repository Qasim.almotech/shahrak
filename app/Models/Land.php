<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Land extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $softDelete = true;

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }

    public function shahrak()
    {
        return $this->belongsTo(Shahrak::class, 'shahrak_id');
    }
}
