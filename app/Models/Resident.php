<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resident extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $softDelete = true;

    public function lands()
    {
        return $this->hasMany(Land::class);
    }

    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }



}
