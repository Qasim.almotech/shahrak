<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shahrak extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $softDelete = true;

    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }

    public function lands()
    {
        return $this->hasMany(Land::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
