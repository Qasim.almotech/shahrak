<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {

            $table->id();
            $table->string('apartment_no');
            $table->integer('shahrak_id');
            $table->integer('resident_id');
            $table->string('block');
            $table->string('tip');
            $table->integer('hall_no');
            $table->string('type');
            $table->integer('rooms');
            $table->integer('bathrooms');
            $table->integer('kitchen');
            $table->integer('floorNo');
            $table->integer('cost');
            $table->integer('percentage');
            $table->integer('net_amount');
            $table->integer('area_size');
            $table->text('east');
            $table->text('west');
            $table->text('north');
            $table->text('south');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
};
