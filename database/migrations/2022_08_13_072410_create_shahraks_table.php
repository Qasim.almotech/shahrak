<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shahraks', function (Blueprint $table) {
            $table->id();
            $table->string('shahrak_name');
            $table->string('no_land');
            $table->string('district');
            $table->integer('total_area_size');
            $table->integer('longtitude');
            $table->integer('latitude');
            $table->string('owner_name');
            $table->string('owner_father_name');
            $table->string('owner_grandfather_name');
            $table->integer('tazkira_number');
            $table->integer('contact_number');
            $table->string('extra_details')->nullable();
            $table->string('contract_company')->nullable();
            $table->string('contract_no')->nullable();
            $table->string('contract_date')->nullable();
            $table->string('sentence_no')->nullable();
            $table->string('contract_department')->nullable();
            $table->string('monitoring')->nullable();
            $table->string('contract_percentage')->nullable();
            $table->string('duration_date')->nullable();
            $table->string('mutch_blocks')->nullable();
            $table->string('other_area_size')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shahraks');
    }
};
