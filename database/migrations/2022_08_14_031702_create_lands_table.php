<?php

use App\Models\Shahrak;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {

            $table->id();
            $table->string('land_no');
            $table->string('district');
            $table->string('east');
            $table->string('west');
            $table->string('north');
            $table->string('south');
            $table->integer('area_size');
            $table->integer('total_price');
            $table->integer('percentage');
            $table->integer('net_amount');
            $table->date('date');
            $table->integer('resident_id');
            $table->integer('shahrak_id');
            $table->string('type');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
};
