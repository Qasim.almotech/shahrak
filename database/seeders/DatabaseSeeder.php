<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // apartment
        Permission::create(['name' => 'apartment_add']);
        Permission::create(['name' => 'apartment_show']);
        Permission::create(['name' => 'apartment_delete']);
        Permission::create(['name' => 'apartment_edit']);
        // shahrak
        Permission::create(['name' => 'shahrak_add']);
        Permission::create(['name' => 'shahrak_show']);
        Permission::create(['name' => 'shahrak_delete']);
        Permission::create(['name' => 'shahrak_edit']);
        // land
        Permission::create(['name' => 'land_add']);
        Permission::create(['name' => 'land_show']);
        Permission::create(['name' => 'land_delete']);
        Permission::create(['name' => 'land_edit']);
        // payment
        Permission::create(['name' => 'payment_add']);
        Permission::create(['name' => 'payment_show']);
        Permission::create(['name' => 'payment_delete']);
        Permission::create(['name' => 'payment_edit']);
        // resident
        Permission::create(['name' => 'resident_add']);
        Permission::create(['name' => 'resident_show']);
        Permission::create(['name' => 'resident_delete']);
        Permission::create(['name' => 'resident_edit']);
        // user
        Permission::create(['name' => 'user_add']);
        Permission::create(['name' => 'user_show']);
        Permission::create(['name' => 'user_delete']);
        Permission::create(['name' => 'user_edit']);
        // user
        Permission::create(['name' => 'document_add']);
        Permission::create(['name' => 'document_show']);
        Permission::create(['name' => 'document_delete']);
        Permission::create(['name' => 'document_edit']);
        // role
        Permission::create(['name' => 'role_add']);
        Permission::create(['name' => 'role_show']);
        Permission::create(['name' => 'role_delete']);
        Permission::create(['name' => 'role_edit']);
        // report
        Permission::create(['name' => 'report_get']);

        $user = User::create([
            'name' => 'qasim',
            'last_name' => 'karimi',
            'position' => 'MIS',
            'contact_number' => '0773542208',
            'email' => 'qasim@km.gov.af',
            'password' => Hash::make('qasim@km.gov.af'),
        ]);

        $role = Role::create(['name' => 'ادمین']);

        $role->syncPermissions(Permission::all());

        $user->syncRoles($role);


    }
}
