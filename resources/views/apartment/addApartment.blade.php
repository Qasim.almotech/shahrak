@extends('app')

@section('page_title',__('general.add_apartment'))

@section('body')
        <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.add_apartment')}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ route('apartment.store')}}" method="POST" id="save">
                    @csrf
                    <div class="row">
                        <div class="col-sm-4">

                    <div class="form-group">
                        <label>{{__('general.resident_name')}} <span class="text-danger"> * </span></label>
                        <select class="form-control select2" style="width: 100%;" id="resident_name" name="resident_id">
                          <option value="">{{__('general.select')}}</option>
                          @foreach ($residents as $resident)
                            <option value="{{ $resident->id }}" @if(old('resident_id') == $resident->id) selected @endif >
                              {{ $resident->NIC.'-'.$resident->name.' '.$resident->father_name.' '.$resident->grandfather_name }}
                            </option>
                          @endforeach
                        </select>
                        @error('resident_id')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>

                      <div class="form-group">
                            <label>{{__('general.shahrak_name')}}<span class="text-danger"> * </span></label>
                            <select class="form-control select2" style="width: 100%;" id='shahrak' name="shahrak_id">
                              <option value="">{{__('general.select')}}</option>
                              @foreach ($shahraks as $id => $shahrakname)
                                  <option value="{{ $id }}" @if(old('shahrak_id') == $id) selected @endif>{{ $shahrakname }}</option>
                              @endforeach
                            </select>
                            @error('shahrak_id')
                                <span style="color: red;">*  {{ $message }} </span>
                            @enderror
                        </div>
                        <div class="form-group">
                          <label>{{__('general.type')}}<span class="text-danger"> * </span></label>
                          <select class="form-control select2" style="width: 100%;" name="type">
                              <option value="">{{__('general.select')}}</option>
                              <option value="government" @if(old('type') == 'government') selected @endif>{{__('general.government')}}</option>
                              <option value="private" @if(old('type') == 'private') selected @endif>{{__('general.private')}} </option>

                          </select>
                          @error('type')
                              <span style="color: red;">*  {{ $message }} </span>
                          @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.ApartmentNo')}}<span class="text-danger"> * </span></label>
                            <input name="ApartmentNo" id="add_apartment" value="{{ old('ApartmentNo') }}"  type="text" class="form-control"  placeholder="Enter Apartment No">
                        @error('ApartmentNo')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('general.tip')}}<span class="text-danger"> * </span></label>
                            <input name="tip" value="{{ old('tip') }}" type="text" class="form-control"  placeholder="Enter Tipe">
                        @error('tip')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Room')}}<span class="text-danger"> * </span></label>
                            <input  name="room" value="{{ old('room') }}"   type="text" class="form-control"  placeholder="Enter Room">
                        @error('room')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                              </div>
                            </div>
                        <div class="col-sm-4">

                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Hall')}}<span class="text-danger"> * </span></label>
                            <input name="hall" value="{{ old('hall') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Hall">
                        @error('hall')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Bathroom')}}<span class="text-danger"> * </span></label>
                            <input name="bathroom" value="{{ old('bathroom') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Bathroom">
                        @error('bathroom')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.kitchen')}}<span class="text-danger"> * </span></label>
                            <input name="kitchen" value="{{ old('kitchen') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter kitchen">
                        @error('kitchen')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Floor')}}<span class="text-danger"> * </span></label>
                            <input name="floor" value="{{ old('floor') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Floor">
                        @error('floor')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Block')}}<span class="text-danger"> * </span></label>
                            <input name="block" value="{{ old('block') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Block">
                        @error('block')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.East')}}</label>
                            <input name="East" value="{{ old('East') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                        @error('East')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.West')}}</label>
                            <input name="West" value="{{ old('West') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                        @error('West')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror</div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.North')}}</label>
                                <input name="North" value="{{ old('North') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter North">
                        @error('North')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror</div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.South')}}</label>
                            <input name="South" value="{{ old('South') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter South">
                        @error('South')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror</div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Areasize')}}<span class="text-danger"> * </span></label>
                            <input name="Areasize" value="{{ old('Areasize') }}" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Area-size">
                        @error('Areasize')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror</div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('language.Totalprice')}}<span class="text-danger"> * </span></label>
                            <input name="Totalprice" value="{{ old('Totalprice') }}" type="number" class="form-control" id="Totalprice" onchange="countAmount()">
                        @error('Totalprice')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror</div>

                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('general.persentage')}}<span class="text-danger"> * </span></label>
                            <input name="percentage" value="{{ old('percentage') }}" type="number" class="form-control" id="percentage" onchange="countAmount()">
                        @error('percentage')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror</div>
                        <div class="form-group col-sm-12">
                            <label for="exampleInputEmail1">{{__('general.net_amount')}}<span class="text-danger"> * </span></label>
                            <input name="NetAmount" value="{{ old('NetAmount') }}"   type="text" class="form-control" id="tax" readonly>
                        @error('NetAmount')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror  </div>
                        <div class="form-group col-sm-12">
                            <label>{{__('language.Date')}}<span class="text-danger"> * </span></label>
                            <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                <input type="text" name="date" class="form-control jalali-date" >

                            </div>
                            @error('date')
                                <span style="color: red;">*  {{ $message }} </span>
                            @enderror
                        </div>

                        </div>

                        <div class="col-1">
                          </div>

                </div>

                  <a type="submit" onclick="confirmsave()" type="submit" class="btn btn-info btn-flat">{{__('general.add_apartment')}}</a>
                </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
</div>
<script>
function confirmsave() {

  let me = document.getElementById('add_apartment').value;
  let shahrak_name = document.getElementById('shahrak');
  let data =  shahrak_name.options[shahrak_name.selectedIndex].text;
  let resodent = document.getElementById('resident_name');
  let data2 =  resodent.options[resident_name.selectedIndex].text;

  let text = 'آیا مطمعین هستید که آپارتمان نمبر  '+me+'  در شهرک  ' +data+'  به نام  '+data2+' ثبت نماید ';
  if (confirm(text) == true) {

    $('form#save').submit();

  }
}
</script>

@endsection

@pushOnce('datatables-script')
<script>
  function countAmount() {
      var tax = ($('#percentage').val() * $('#Totalprice').val()) / 100;

      $('#tax').val(tax);
  }
</script>
@endpushOnce
