@extends('app')

@section('page_title',__('general.edit_apartment'))

@section('body')
        <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.edit_apartment')}}</h3>
            </div>

            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ route('apartment.update', $apartment->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-sm-4">

                    <div class="form-group">
                        <label>{{__('general.resident_name')}}<span class="text-danger"> * </span></label>
                        <select class="form-control select2" style="width: 100%;" name="resident_id">
                          <option value="">{{__('general.select')}}</option>
                          @foreach ($residents as $resident)
                            <option value="{{ $resident->id }}" @if($apartment->resident_id == $resident->id) selected @endif >
                              {{ $resident->NIC.'-'.$resident->name.' '.$resident->father_name.' '.$resident->grandfather_name }}
                            </option>
                          @endforeach
                        </select>
                        @error('resident_id')
                           <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>{{__('general.shahrak_name')}}<span class="text-danger"> * </span></label>
                        <select class="form-control select2" style="width: 100%;" name="shahrak_id">
                          <option value="">{{__('general.select')}}</option>
                          @foreach ($shahraks as $id => $shahrakname)
                            <option value="{{ $id }}" @if($apartment->shahrak_id == $id) selected @endif >{{ $shahrakname }}</option>
                          @endforeach
                        </select>
                        @error('shahrak_id')
                           <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>{{__('general.type')}}<span class="text-danger"> * </span></label>
                        <select class="form-control select2" style="width: 100%;" name="type">
                            <option value="">{{__('general.select')}}</option>
                            <option value="government" @if($apartment->type == 'government') selected @endif>{{__('general.government')}}</option>
                            <option value="private" @if($apartment->type == 'private') selected @endif>{{__('general.private')}} </option>
                            @error('type')
                               <span style="color: red;">*  {{ $message }} </span>
                            @enderror
                        </select>
                      </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.ApartmentNo')}}<span class="text-danger"> * </span></label>
                                <input name="ApartmentNo" value="{{ $apartment->apartment_no }}"  type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Apartment No">
                                 @error('ApartmentNo')
                                      <span style="color: red;">*  {{ $message }} </span>
                                  @enderror
                              </div>
                              <div class="form-group col-sm-12">
                                  <label for="exampleInputEmail1">{{__('general.tip')}}<span class="text-danger"> * </span></label>
                                  <input name="tip" value="{{ $apartment->tip }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Tipe">
                                  @error('tip')
                                    <span style="color: red;">*  {{ $message }} </span>
                                  @enderror
                                </div>

                                  <div class="form-group col-sm-12">
                                      <label for="exampleInputEmail1">{{__('language.Room')}}<span class="text-danger"> * </span></label>
                                      <input  name="room" value="{{ $apartment->rooms }}"   type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Room">
                                      @error('room')
                                      <span style="color: red;">*  {{ $message }} </span>
                                      @enderror
                                  </div>
                            </div>
                        <div class="col-sm-4">

                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.Hall')}}<span class="text-danger"> * </span></label>
                                <input name="hall" value="{{ $apartment->hall_no }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Hall">
                            @error('hall')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.Bathroom')}}<span class="text-danger"> * </span></label>
                                <input name="bathroom" value="{{ $apartment->bathrooms }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Bathroom">
                            @error('bathroom')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.kitchen')}}<span class="text-danger"> * </span></label>
                                <input name="kitchen" value="{{ $apartment->kitchen }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter kitchen">
                             @error('kitchen')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.Floor')}}<span class="text-danger"> * </span></label>
                                <input name="floor" value="{{ $apartment->floorNo }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Floor">
                            @error('floor')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.Block')}}<span class="text-danger"> * </span></label>
                                <input name="block" value="{{ $apartment->block }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Block">
                            @error('block')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.East')}}</label>
                                <input name="East" value="{{ $apartment->east }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                                @error('East')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror
                                </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.West')}}</label>
                                <input name="West" value="{{$apartment->west }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                            @error('West')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror</div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.North')}}</label>
                                <input name="North" value="{{ $apartment->north }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter North">
                             @error('North')
                                            <span style="color: red;">*  {{ $message }} </span>
                                            @enderror</div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.South')}}</label>
                                <input name="South" value="{{ $apartment->south }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter South">
                             @error('South')
                                <span style="color: red;">*  {{ $message }} </span>
                            @enderror</div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.Areasize')}}<span class="text-danger"> * </span></label>
                                <input name="Areasize" value="{{ $apartment->area_size }}" type="number" class="form-control" >
                            @error('Areasize')
                                <span style="color: red;">*  {{ $message }} </span>
                            @enderror</div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('language.Totalprice')}}<span class="text-danger"> * </span></label>
                                <input name="Totalprice" value="{{ $apartment->cost }}" type="number" class="form-control" id="Totalprice" onchange="countAmount()" >
                                @error('Totalprice')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('general.persentage')}}<span class="text-danger"> * </span></label>
                                <input name="percentage" value="{{ $apartment->percentage }}" type="number" class="form-control" id="percentage" onchange="countAmount()" >
                                @error('percentage')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                              </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputEmail1">{{__('general.net_amount')}}<span class="text-danger"> * </span></label>
                                <input name="NetAmount" value="{{ $apartment->net_amount }}"   type="text" class="form-control" id="tax" readonly >
                                @error('NetAmount')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <label>{{__('language.Date')}}<span class="text-danger"> * </span></label>
                                  <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                      <input value="{{ $apartment->date }}" type="text" class="form-control jalali-date" name="date">

                                  </div>
                                  @error('date')
                                      <span style="color: red;">*  {{ $message }} </span>
                                  @enderror
                            </div>
                        </div>
                        <div class="col-1">
                        </div>
                </div>
                <button type="submit" class="btn btn-info btn-flat">{{__('general.add_apartment')}}</button>
                </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>

</div>


@endsection

@pushOnce('datatables-script')
<script>
  function countAmount() {
      var tax = ($('#percentage').val() * $('#Totalprice').val()) / 100;

      $('#tax').val(tax);
  }
</script>
@endpushOnce
