@extends('app')

@section('page_title','مشخصات آپارتمان')

@section('body')

<div class="card">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <!-- /.card -->

        
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <table class="table table-sm table-bordered" >
                  <h5>{{__('general.apartment_info')}}</h5>
                  <tbody>
                    <tr class="tr">

                      <th class="th" id="name">{{__('general.owner_name')}}</th>
                            <td class="td"> {{ $apartment->resident->name;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.father_name')}}</th>
                            <td class="td"> {{ $apartment->resident->father_name;}}</td>

                        </tr>
                        <tr class="tr">
                          <th class="th" id="name">{{__('general.grandfather_name')}}</th>
                            <td class="td"> {{ $apartment->resident->grandfather_name;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.province')}}</th>
                            <td class="td"> {{ $apartment->resident->province;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.district')}}</th>
                            <td class="td"> {{ $apartment->resident->district;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.village')}}</th>
                            <td class="td"> {{ $apartment->resident->village;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.NIC')}}</th>
                            <td class="td"> {{ $apartment->resident->NIC;}}</td>

                        </tr>
                         <tr class="tr">

                          <th class="th" id="name">{{__('general.book_no')}}</th>
                            <td class="td"> {{ $apartment->resident->book;}}</td>

                        </tr>
                         <tr class="tr">

                          <th class="th" id="name">{{__('general.page_no')}}</th>
                            <td class="td"> {{ $apartment->resident->page;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.ap_no')}}</th>
                            <td class="td"> {{ $apartment->apartment_no;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.shahrak_name')}}</th>
                            <td class="td"> {{ $apartment->shahrak->shahrak_name;}}</td>

                        </tr>

                        <tr class="tr">

                          <th class="th" id="name">{{__('general.area_size')}}</th>
                            <td class="td"> {{ $apartment->area_size;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.cost')}}</th>
                            <td class="td"> {{ $apartment->cost;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.persentage')}}</th>
                            <td class="td"> {{ $apartment->percentage;}}</td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.four_sides')}}</th>
                            <td class="td"> شرق: {{ $apartment->east;}} غرب: {{ $apartment->west;}} شمال: {{ $apartment->north;}} جنوب: {{ $apartment->south;}} </td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.apartment_info')}}</th>
                            <td class="td">  اتاق: {{ $apartment->rooms;}} آشپزخانه: {{ $apartment->kitchen;}} تشناب: {{ $apartment->bathrooms;}} </td>

                        </tr>
                        <tr class="tr">

                          <th class="th" id="name">{{__('general.date')}}</th>
                            <td class="td"> {{ $apartment->date;}}</td>

                        </tr>


                  </tbody>
                </table>
              </div>
              <div class="col-md-6">
                <h5>پرداخت ها </h5>
                <table class="table table-sm table-bordered" >
                  <thead>
                    <tr>

                      <th>قیمت</th>
                      <th>قسط</th>
                      <th>آویز</th>
                      <th>تاریخ</th>


                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($apartment->payments as $payment)
                    <tr>
                    <td>{{$payment->amount }}</td>
                    <td>{{$payment->partial_no }}</td>
                    <td>{{$payment->awiz_no }}</td>
                    <td>
                      @php
                        $date = explode('-', $payment->date);
                        echo gregorian_to_jalali($date[0], $date[1], $date[2]);
                      @endphp
                    </td>
                  </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>









            </div>



        </div>
      </div>
    </div>
    <!-- /.card-body -->
  </div>



@endsection
