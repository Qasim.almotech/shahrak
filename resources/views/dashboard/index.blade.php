
@extends('app')

@section('page_title', __('general.dashboard'))

@section('body')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3>{{__('general.shahraks')}}</h3>

            <h2>{{ $shahrak->count() }}</h2>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="{{ route('shahrak.index') }}" class="small-box-footer">{{ __('general.more_info') }}<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>{{__('general.apartments')}}<sup style="font-size: 20px"></sup></h3>

            <h2>{{ $apartments->count() }}</h2>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="{{ route('apartment.index') }}" class="small-box-footer">{{ __('general.more_info') }}<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
          <div class="inner">
            <h3>{{__('general.lands')}}</h3>

            <h2>{{ $lands->count() }}</h2>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="{{ route('land.index') }}" class="small-box-footer"> {{ __('general.more_info') }} <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>{{__('general.residents')}}</h3>

            <h2>{{ $resident->count() }}</h2>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{ route('resident.index') }}" class="small-box-footer">{{ __('general.more_info') }} <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <div class="row">
      <div class="col-md-6">
    <div class="card card-danger">
        <div class="card-header">
          <h3 class="card-title">{{__('general.apartment_chart')}}</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
          <div id="apartmentpiechart"></div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
    <div class="col-md-6">
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">{{__('general.land_chart')}}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div id="landcolumnChart"></div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">{{__('general.land_chart')}}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div id="PrivateGovernment"></div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
</div>
   </div>






@endsection
@push('chartScript')
<script>
  $(document).ready(function () {
    countland();
    countapartment();
    privateandgovernment();
    
  });
  function countland() {//this function is use for Geting all land from a shahrak
        $.ajax({
            url: "countland",
            type: "GET",
            data: {},
            success: function (result) {
              
                var TotalEstimated = 0;
                var aData = result;
                var arr = []
                $.map(aData, function (item, index) {
                     var obj = {};
                     obj.name = item.shahrak_name;
                     obj.y = item.landID;
                    arr.push(obj);
                });
                var myJsonString = JSON.stringify(arr);
                var jsonArray = JSON.parse(JSON.stringify(arr));
                landcolumnChart(jsonArray);

            },
            error: function (error) {
                alert(error.responseText);
                
            },

        });
    }




  function landcolumnChart(jsonArray) { //this function make a column charts for land
    Highcharts.getOptions().plotOptions.pie.colors = ['#2f7ed8', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a', '#047CF9', '#910000', '#8bbc21', '#D91E18', '#26C281', '#9A12B3', '#9B59B6', '#E87E04', '#C49F47'];
      Highcharts.chart('landcolumnChart', {
          chart: {
              backgroundColor: 'white',
              polar: true,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'column',
              options3d: {
                  enabled: true,
                  alpha: 45,
                  beta: 0,
                  depth: 50,
              }
          },
          title: {
              text: 'Land'
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },

          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0,
                  dataLabels: {
                      enabled: true,
                      format: '{point.name}',
                      depth: 25
                  },
                  showInLegend: true,
              },
          },

          series: [{
              name: 'Total Land',
              colorByPoint: true,
               data: jsonArray
              
          }]
      });
  }

  function countapartment() {//this function is use for Geting all apartment from a shahrak
        $.ajax({
            url: "countapartment",
            type: "GET",
            data: {},
            success: function (result) {
              
                var TotalEstimated = 0;
                var aData = result;
                var arr = []
                $.map(aData, function (item, index) {
                  
                     var obj = {};
                     obj.name = item.shahrak_name;
                     obj.y = item.ApartmentID;
                    arr.push(obj);
                });
                var myJsonString = JSON.stringify(arr);
                var jsonArray = JSON.parse(JSON.stringify(arr));
                apartmentpiechart(jsonArray);
                // console.log(jsonArray);

            },
            error: function (error) {
                alert(error.responseText);
                
            },

        });
    }
  function apartmentpiechart(jsonArray) {//this function make a pie chart for Apartment
    Highcharts.getOptions().plotOptions.pie.colors = ['#2f7ed8', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a', '#047CF9', '#910000', '#8bbc21', '#D91E18', '#26C281', '#9A12B3', '#9B59B6', '#E87E04', '#C49F47'];
      Highcharts.chart('apartmentpiechart', {
          chart: {
              backgroundColor: 'white',
              polar: true,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie',
              options3d: {
                  enabled: true,
                  alpha: 45,
                  beta: 0,
                  depth: 50,
              }
          },
          title: {
              text: 'Apartment'
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },

          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0,
                  dataLabels: {
                      enabled: true,
                      format: '{point.name}',
                      depth: 25
                  },
                  showInLegend: true,
              },
          },

          series: [{
              name: 'Total Apartment',
              colorByPoint: true,
               data: jsonArray
              
          }]
      });
  }

  function privateandgovernment() {//this function is use to shwo private and government  apartment and land 
        $.ajax({
            url: "privateandgovernment",
            type: "GET",
            data: {},
            success: function (result) {
            console.log(result);
                var TotalEstimated = 0;
                var aData = result;
                var arr = [];
                $.map(aData, function (item, index) {
                  var obj = {};
                    obj.name = index;
                    obj.y = item;
                    arr.push(obj);
                });
                var myJsonString = JSON.stringify(arr);
                var jsonArray = JSON.parse(JSON.stringify(arr));
                privateandgovernmentcolumnchart(jsonArray);
               

            },
            error: function (error) {
                alert(error.responseText);
                
            },

        });
    }
  function privateandgovernmentcolumnchart(jsonArray) {//this function make a column  chart for Apartment and land with private and government
    console.log(jsonArray);
    Highcharts.getOptions().plotOptions.pie.colors = ['#2f7ed8', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a', '#047CF9', '#910000', '#8bbc21', '#D91E18', '#26C281', '#9A12B3', '#9B59B6', '#E87E04', '#C49F47'];
      Highcharts.chart('PrivateGovernment', {
          chart: {
              backgroundColor: 'white',
              polar: true,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'column',
              options3d: {
                  enabled: true,
                  alpha: 45,
                  beta: 0,
                  depth: 50,
              }
          },
          title: {
              text: 'زمین و اپارتمان شخصی و دولتی'
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },

          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0,
                  dataLabels: {
                      enabled: true,
                      format: '{point.name}',
                      depth: 25
                  },
                  showInLegend: true,
              },
          },

          series: [{
              name: 'a',
              colorByPoint: true,
               data: jsonArray
              }
            ]
      });
  }

</script>
@endpush
