@extends('app')

@section('page_title',__('general.add_document'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.add_document')}}</h3>
            </div>

            <div class="card-body">
              <form action="{{ route('document.store') }}" method="POST" enctype='multipart/form-data'>
                @csrf
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="type"  value="apartment" >
                        {{__('general.is_apartment')}}
                      </label>
                      <label>
                        <input type="radio" name="type"  value="land" >
                        {{__('general.is_land')}}
                      </label>
                      <label>
                        <input type="radio" name="type"  value="shahrak" >
                        {{__('general.is_shahrak')}}
                      </label>
                    </div>
                    <div class="form-group hidden land">
                      <label>{{__('general.land_name')}}</label>

                      <select class="form-control select2" style="width: 100%;" name="land_id">
                          <option>{{__('general.select')}}</option>
                          @foreach ($lands as $id => $land)
                            <option value="{{ $land->id }}">{{ $land->land_no.'-'.$land->resident->name.' '.$land->resident->father_name }}</option>
                          @endforeach
                      </select>
                    </div>

                    <div class="form-group hidden apartment">
                      <label>{{__('general.apartment_name')}}</label>

                      <select class="form-control select2" style="width: 100%;" name="apartment_id">
                          <option>{{__('general.select')}}</option>
                          @foreach ($apartments as $id => $apartment)
                            <option value="{{ $apartment->id }}">{{ $apartment->apartment_no.'-'.$apartment->resident->name.' '.$apartment->resident->father_name }}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="form-group hidden shahrak">
                      <label>{{__('general.shahrak')}}</label>

                      <select class="form-control select2" style="width: 100%;" name="shahrak_id">
                          <option>{{__('general.select')}}</option>
                          @foreach ($shahrak as $id => $shahrak)
                            <option value="{{ $shahrak->id }}">{{ $shahrak->shahrak_name}}</option>
                          @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputFile">{{__('general.choose_file')}}<span class="text-danger"> * </span></label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input"  name="file" value="{{ old('file') }}"> 
                          <label class="custom-file-label" >{{__('general.choose_file')}}</label>
                          
                        </div>
                        
                      </div>
                      @error('file')
                            <span style="color: red;">*  {{ $message }} </span>
                          @enderror
                    </div>

                    <div class="form-group">
                      <label>{{__('general.type')}}<span class="text-danger"> * </span></label>
                      <select class="form-control select2" style="width: 100%;" name="type_id" >
                          <option value="">{{__('general.select')}}</option>
                          <option value="maktoob_shahrak" @if(old('type_id') == 'maktoob_shahrak') selected @endif>{{__('docs.maktoob_shahrak') }} </option>
                          <option value="maktoob_qabala" @if(old('type_id') == 'maktoob_qabala') selected @endif>{{__('docs.maktoob_qabala') }}</option>
                          <option value="maktoob_walayat" @if(old('type_id') == 'maktoob_walayat') selected @endif>{{__('docs.maktoob_walayat') }}</option>
                          <option value="maktoob_awayed" @if(old('type_id') == 'maktoob_awayed') selected @endif>{{__('docs.maktoob_awayed') }}</option>
                          <option value="maktoob_awiz" @if(old('type_id') == 'maktoob_awiz') selected @endif>{{__('docs.maktoob_awiz') }}</option>
                          <option value="tazkira_resident" @if(old('type_id') == 'tazkira_resident') selected @endif>{{__('docs.tazkira_resident') }}</option>

                      </select>
                      @error('type_id')
                              <span style="color: red;">*  {{ $message }} </span>
                          @enderror
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">{{__('general.date')}} <span class="text-danger"> * </span></label>
                      <input value="{{ old('date') }}" type="text" class="form-control jalali-date"  name="date">
                    </div>
                    <div class="form-group">
                      <label >{{__('general.details')}}</label>
                      <textarea class="form-control" name="details" rows="4"></textarea>
                      @error('bill_no')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>

                  </div>

                  </div>
                  <button type="submit" class="btn btn-info btn-flat">{{__('general.add_document')}}</button>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
@endsection

@pushOnce('datatables-script')
<script>
  $('input[type=radio][name=type]').change(function() {
      if (this.value == 'land') {

          $(".land").css("display", "block");
          $(".apartment").css("display", "none");
          $(".shahrak").css("display", "none");
      }
      else if (this.value == 'apartment') {

          $(".apartment").css("display", "block");
          $(".land").css("display", "none");
          $(".shahrak").css("display", "none");
      }
              else if (this.value == 'shahrak') {

        $(".apartment").css("display", "none");
        $(".land").css("display", "none");
        $(".shahrak").css("display", "block");
        }

  });

</script>

@endpushOnce
