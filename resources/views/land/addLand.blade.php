@extends('app')

@section('page_title',__('general.add_land'))

@section('body')
<div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title rtl">{{__('general.add_land')}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ route('land.store') }}" method="POST" id="save">
            @csrf
            <div class="row">

                <div class="col-sm-4">

                    <div class="form-group">
                        <label>{{__('general.resident_name')}}<span class="text-danger"> * </span></label>
                        <select class="form-control select2" style="width: 100%;" id="resident_name" name="resident_id">
                          <option value="">{{__('general.select')}}</option>
                          @foreach ($residents as $resident)
                              <option value="{{ $resident->id }}"  @if(old('resident_id') == $resident->id) selected @endif>
                                {{ $resident->NIC.'-'.$resident->name.' '.$resident->father_name.' '.$resident->grandfather_name }}
                              </option>
                          @endforeach
                        </select>
                        @error('resident_id')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>

                <div class="form-group">
                    <label>{{__('general.shahrak_name')}}<span class="text-danger"> * </span></label>
                    <select class="form-control select2" style="width: 100%;" id="shahrak" name="shahrak_id">
                      <option value="">{{__('general.select')}}</option>

                      @foreach ($shahraks as $id => $shahrakname)
                      <option value="{{ $id }}"  @if(old('shahrak_id') == $id) selected @endif >{{ $shahrakname }}</option>
                    @endforeach
                    </select>
                    @error('shahrak_id')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label>{{__('general.type')}}<span class="text-danger"> * </span></label>
                    <select class="form-control select2" style="width: 100%;" name="type">
                        <option value="">{{__('general.select')}}</option>
                        <option value="government"  @if(old('type') == 'government') selected @endif>{{__('general.government')}}</option>
                        <option value="private" @if(old('type') == 'private') selected @endif>{{__('general.private')}}</option>
                    </select>
                    @error('type')
                    <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                  </div>
                  <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('general.land_no')}}<span class="text-danger"> * </span></label>
                        <input name="landno" value="{{ old('landno') }}" id="add_land" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter land No">
                        @error('landno')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                  </div>
                  <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.District')}}<span class="text-danger"> * </span></label>
                        <input name="district" value="{{ old('district') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter district">
                        @error('district')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>

                </div>
                <div class="col-sm-4">

                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.East')}}</label>
                        <input name="East" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter East">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.West')}}</label>
                        <input name="West" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.North')}}</label>
                        <input name="North" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter North">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.South')}}</label>
                        <input name="South" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter South">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.Areasize')}}<span class="text-danger">*</span></label>
                        <input name="Areasize" value="{{ old('Areasize') }}" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Area-size">
                        @error('Areasize')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>

                </div>
                <div class="col-sm-4">

                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.Totalprice')}}<span class="text-danger"> * </span></label>
                        <input name="Totalprice" value="{{ old('Totalprice') }}" type="number" class="form-control" onchange="countAmount()" id="Totalprice" >
                        @error('Totalprice')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('general.persentage')}}<span class="text-danger"> * </span></label>
                        <input name="percentage" value="{{ old('percentage') }}" type="number" class="form-control" onchange="countAmount()" id="percentage">
                        @error('percentage')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('general.net_amount')}}<span class="text-danger"> * </span></label>
                        <input name="net_amount" value="{{ old('net_amount') }}" type="text" class="form-control" id="tax" readonly>
                        @error('net_amount')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12">
                        <label>{{__('language.Date')}}<span class="text-danger"> * </span></label>

                        <input name="date" type="text" class="form-control jalali-date" data-target="#reservationdatetime">
                        @error('date')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror


                    </div>


                </div>

        </div>
        <a  onclick="confirmsave()" class="btn btn-info btn-flat">{{__('general.add_land')}}</a>
        </div>
        </form>
    </div>
    <!-- /.card-body -->
</div>
</div>
<script>
function confirmsave() {

  let me = document.getElementById('add_land').value;
  let shahrak_name = document.getElementById('shahrak');
  let data =  shahrak_name.options[shahrak_name.selectedIndex].text;
  let resodent = document.getElementById('resident_name');
  let data2 =  resodent.options[resident_name.selectedIndex].text;
  let text = 'آیا مطمعین هستید که زمین نمبر  '+me+'  در شهرک  ' +data+'  به نام  '+data2+' ثبت نماید ';

  if (confirm(text) == true) {

    $('form#save').submit();

  }
}
</script>

@endsection

@pushOnce('datatables-script')
<script>
  function countAmount() {
      var tax = ($('#percentage').val() * $('#Totalprice').val()) / 100;
      // alert(tax);
      $('#tax').val(tax);
  }
</script>

@endpushOnce
