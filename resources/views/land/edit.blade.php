@extends('app')

@section('page_title',__('general.edit_land'))

@section('body')
<div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title rtl">{{__('general.edit_land')}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ route('land.update', $land->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">

                <div class="col-sm-4">

                    <div class="form-group">
                        <label>{{__('general.resident_name')}}<span class="text-danger"> * </span></label>
                        <select class="form-control select2" style="width: 100%;" name="resident_id">
                          <option>{{__('general.select')}}</option>
                          @foreach ($residents as $resident)
                              <option value="{{ $resident->id }}" @if($land->resident_id == $resident->id) selected @endif >
                                {{ $resident->NIC.'-'.$resident->name.' '.$resident->father_name.' '.$resident->grandfather_name }}
                              </option>
                          @endforeach
                        </select>
                    </div>

                  <div class="form-group">
                      <label>{{__('general.shahrak_name')}}<span class="text-danger"> * </span></label>
                      <select class="form-control select2" style="width: 100%;" name="shahrak_id">
                        <option>{{__('general.select')}}</option>
                        @foreach ($shahraks as $id => $shahrakname)
                          <option value="{{ $id }}"  @if($land->shahrak_id == $id) selected @endif >{{ $shahrakname }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label>{{__('general.type')}}<span class="text-danger"> * </span></label>
                      <select class="form-control select2" style="width: 100%;" name="type">
                        <option>{{__('general.select')}}</option>
                        <option value="government" @if($land->type == 'government') selected @endif>{{__('general.government')}}</option>
                        <option value="private" @if($land->type == 'private') selected @endif>{{__('general.private')}} </option>

                      </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('general.land_no')}}<span class="text-danger"> * </span></label>
                        <input value="{{ $land->land_no }}" name="landno" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter land No">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.District')}}<span class="text-danger"> * </span></label>
                        <input value="{{ $land->district }}" name="district" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter district">
                    </div>




                </div>
                <div class="col-sm-4">

                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.East')}}</label>
                        <input value="{{ $land->east }}" name="East" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.West')}}</label>
                        <input value="{{ $land->west }}" name="West" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter West">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.North')}}</label>
                        <input value="{{ $land->north }}" name="North" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter North">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.South')}}</label>
                        <input value="{{ $land->south }}" name="South" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter South">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.Areasize')}}<span class="text-danger"> * </span></label>
                        <input value="{{ $land->area_size }}" name="Areasize" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Area-size">
                    </div>


                </div>
                <div class="col-sm-4">

                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('language.Totalprice')}}<span class="text-danger"> * </span></label>
                        <input value="{{ $land->total_price }}" name="Totalprice" type="number" class="form-control" id="Totalprice" onchange="countAmount()" >
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('general.persentage')}}<span class="text-danger"> * </span></label>
                        <input value="{{ $land->percentage }}" name="percentage" type="number" class="form-control" id="percentage" onchange="countAmount()" >
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleInputEmail1">{{__('general.net_amount')}}<span class="text-danger"> * </span></label>
                        <input value="{{ $land->net_amount }}" name="net_amount" type="text" class="form-control" id="tax" readonly >
                    </div>
                    <div class="form-group col-sm-12">
                        <label>{{__('language.Date')}}<span class="text-danger"> * </span></label>
                        <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                            <input name="date" type="text" value="{{ $land->date }}" class="form-control jalali-date" data-target="#reservationdatetime">

                        </div>
                    </div>
                </div>
        </div>
        <button type="submit" class="btn btn-info btn-flat">{{__('general.add_land')}}</button>
        </div>
        </form>
    </div>
    <!-- /.card-body -->
</div>


</div>

@endsection

@pushOnce('datatables-script')
<script>
  function countAmount() {
      var tax = ($('#percentage').val() * $('#Totalprice').val()) / 100;
      // alert(tax);
      $('#tax').val(tax);
  }
</script>
@endpushOnce
