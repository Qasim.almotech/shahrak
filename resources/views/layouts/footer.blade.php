
  <footer class="main-footer">
        <strong>{{__('general.footer_copyright')}}</strong>

</footer>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- date-range-picker -->
<!-- <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- AdminLTE for demo purposes -->

<!-- <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script> -->

<link rel="stylesheet" href="{{asset('farsi/persian-datepicker.min.css')}}"/>
  <script src="{{asset('farsi/jquery.min.js')}}"></script>
  <script src="{{asset('farsi/jquery.js')}}"></script>
  <script src="{{asset('farsi/persian-date.min.js')}}"></script>
  <script src="{{asset('farsi/persian-datepicker.min.js')}}"></script>
  <script src="{{ asset('HighCharts/highcharts.js') }}"></script>
  <script>
    $(document).ready(function() {
    $(".jalali-date").pDatepicker({
      initialValueType: "gregorian",
      format: "YYYY/MM/DD",
      onSelect: "year"
    });
  });
  </script>



</body>
</html>
