<!-- Main Sidebar Container -->
  <!-- Brand Logo -->


  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ __('general.kabul_municipality') }}</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->


    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">

          @if(Route::current()->getName() == 'dashboard')
          <a href="{{ route('dashboard') }}" class="nav-link active">
            @else

          <a href="{{ route('dashboard') }}" class="nav-link">
            @endif
            <i class="nav-icon fas fa-desktop"></i>
            <p>
              {{__('general.dashboard')}}
            </p>
          </a>

        </li>


        @canany(['shahrak_add', 'shahrak_show'])
          <li class="nav-item">

            <a href="#" class="nav-link
            @if(Route::current()->getName() == 'shahrak.create' || Route::current()->getName() == 'shahrak.index')
            active
            @endif
            ">
            <i class="nav-icon fas fa-university"></i>
              <p> {{ __('general.shahraks') }}
                <i class="fas fa-angle-left right"></i>
              </p>
          </a>
          <ul class="nav nav-treeview" style="
            @if(Route::current()->getName() == 'shahrak.create' || Route::current()->getName() == 'shahrak.index')
          display: block;
          @else
          display: none;
          @endif
          ">
          @can('shahrak_show')
            <li class="nav-item">
              <a href="{{ route('shahrak.index') }}" class="nav-link
              @if(Route::current()->getName() == 'shahrak.index')
              active
              @endif
              ">
                <i class="far fa-circle nav-icon"></i>
                <p>{{ __('general.shahrak_list') }}</p>
              </a>
            </li>
          @endcan
          @can('shahrak_add')
            <li class="nav-item">
                  <a href="{{ route('shahrak.create') }}" class="nav-link
                  @if(Route::current()->getName() == 'shahrak.create')
                  active
                  @endif
                  ">
                    <i class="far fa-circle nav-icon"></i>
                    <p> {{__('general.add_shahrak') }}</p>
                </a>
            </li>
          @endcan
        </ul>
      </li>
      @endcanany



      @canany(['resident_add', 'resident_show'])
         <li class="nav-item">
              <a href="#" class="nav-link
                @if(Route::current()->getName() == 'resident.create' || Route::current()->getName() == 'resident.index')
                active
                @endif
                ">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  {{ __('general.residents') }}
                <i class="fas fa-angle-left right"></i>
                </p>
              </a>
          <ul class="nav nav-treeview" style="
            @if(Route::current()->getName() == 'resident.create' || Route::current()->getName() == 'resident.index')
              display: block;
            @else
              display: none;
            @endif
          ">
          @can('resident_show')
            <li class="nav-item">
                <a href="{{ route('resident.index') }}" class="nav-link
                @if(Route::current()->getName() == 'resident.index')
                active
                @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('general.resident_list') }}</p>
                </a>
            </li>
          @endcan
          @can('resident_add')
            <li class="nav-item">
                  <a href="{{ route('resident.create') }}" class="nav-link
                  @if(Route::current()->getName() == 'resident.create')
                  active
                  @endif
                  ">

                    <i class="far fa-circle nav-icon"></i>
                    <p>
                    {{__('general.add_resident')}}
                  </p>
                </a>
            </li>
          @endcan
        </ul>
      </li>
    @endcanany

    @canany(['land_show','land_add'])
     <li class="nav-item">

        <a href="#" class="nav-link
          @if(Route::current()->getName() == 'land.create' || Route::current()->getName() == 'land.index')
          active
          @endif
        ">
          <i class="nav-icon fas fa-globe"></i>
          <p>
          {{ __('general.lands') }}
          <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview" style="
         @if(Route::current()->getName() == 'land.create' || Route::current()->getName() == 'land.index')
            display: block;
         @else
            display: none;
          @endif
        ">
          @can('land_show')
            <li class="nav-item">
            <a href="{{ route('land.index') }}" class="nav-link
            @if(Route::current()->getName() == 'land.index')
            active
            @endif
            ">
              <i class="far fa-circle nav-icon"></i>
              <p>{{ __('general.land_list') }}</p>
            </a>
            </li>
          @endcan
          @can('land_add')
            <li class="nav-item">
                  <a href="{{ route('land.create') }}" class="nav-link
                  @if(Route::current()->getName() == 'land.create')
                  active
                  @endif
                  ">


                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    {{__('general.add_land')}}
                </p>
                </a>
            </li>
          @endcan
        </ul>
      </li>
    @endcanany


    @canany(['apartment_show','apartment_add'])
      <li class="nav-item">
        <a href="#" class="nav-link
          @if(Route::current()->getName() == 'apartment.create' || Route::current()->getName() == 'apartment.index')
          active
          @endif
          ">
          <i class="nav-icon fas fa-building"></i>
          <p>
          {{__('general.apartments')}}
          <i class="fas fa-angle-left right"></i>
          </p>
        </a>
      <ul class="nav nav-treeview" style="
       @if(Route::current()->getName() == 'apartment.create' || Route::current()->getName() == 'apartment.index')
          display: block;
                @else
                display: none;
                @endif
      ">
        @can('apartment_show')
          <li class="nav-item">
          <a href=" {{ route('apartment.index') }}" class="nav-link
          @if(Route::current()->getName() == 'apartment.index')
          active
          @endif
          ">
            <i class="far fa-circle nav-icon"></i>
            <p>{{__('general.apartment_list')}}</p>
          </a>
          </li>
        @endcan
        @can('apartment_add')
          <li class="nav-item">
              <a href="{{ route('apartment.create') }}" class="nav-link
                @if(Route::current()->getName() == 'apartment.create')
                  active
                @endif
                ">
                <i class="far fa-circle nav-icon"></i>
                <p>
                    {{__('general.add_apartment')}}
                </p>
              </a>
          </li>
        @endcan
      </ul>
    </li>
  @endcanany

  @canany(['payment_add', 'payment_show'])
    <li class="nav-item">
      <a href="#" class="nav-link
      @if(Route::current()->getName() == 'payment.create' || Route::current()->getName() == 'payment.index')
      active
      @endif
      ">
        <i class="nav-icon fa fa-credit-card"></i>
        <p>
        {{__('general.payments')}}
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview" style="
      @if(Route::current()->getName() == 'payment.create' || Route::current()->getName() == 'payment.index')
         display: block;
      @else
         display: none;
      @endif
     ">
     @can('payment_show')
        <li class="nav-item">
          <a href="{{ Route('payment.index') }}" class="nav-link
            @if(Route::current()->getName() == 'payment.index')
              active
            @endif
          ">
            <i class="far fa-circle nav-icon"></i>
            <p>{{__('general.list_payment')}}</p>
          </a>
        </li>
      @endcan
      @can('payment_add')
        <li class="nav-item">
          <a href="{{ route('payment.create') }}" class="nav-link
          @if(Route::current()->getName() == 'payment.create')
            active
          @endif
            ">
            <i class="far fa-circle nav-icon"></i>
            <p>{{__('general.add_payment')}}</p>
          </a>
        </li>
      @endcan
      </ul>
    </li>
    @endcanany

    @canany(['user_add', 'user_show'])
    <li class="nav-item">
      <a href="#" class="nav-link
          @if(Route::current()->getName() == 'register' || Route::current()->getName() == 'user.index')
            active
          @endif
          " >
        <i class="nav-icon fas fa-user"></i>
        <p>
        {{__('general.users')}}
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview" style="
        @if(Route::current()->getName() == 'register' || Route::current()->getName() == 'user.index')
           display: block;
        @else
           display: none;
        @endif
      ">
        @can('user_show')
          <li class="nav-item">
            <a href="{{ route('user.index') }}" class="nav-link
              @if(Route::current()->getName() == 'user.index')
                active
              @endif
            ">
              <i class="far fa-circle nav-icon"></i>
              <p>{{__('general.list_users')}}</p>
            </a>
          </li>
        @endcan
        @can('user_add')
          <li class="nav-item">
            <a href="{{ route('register') }}" class="nav-link
              @if(Route::current()->getName() == 'register')
                active
              @endif
            ">
              <i class="fa fa-user-plus nav-icon"></i>
              <p>{{__('general.add_user')}}</p>
            </a>
          </li>
        @endcan
      </ul>
    </li>
    @endcanany

    @canany(['document_show', 'document_add'])
    <li class="nav-item">
      <a href="#" class="nav-link
        @if(Route::current()->getName() == 'document.index' || Route::current()->getName() == 'document.create')
          active
        @endif
      ">
        <i class="nav-icon fas fa-cogs"></i>
        <p>
          {{__('general.documents')}}
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview" style="
        @if(Route::current()->getName() == 'document.index' || Route::current()->getName() == 'document.create')
           display: block;
        @else
           display: none;
        @endif
      ">
        @can('document_show')
          <li class="nav-item">
            <a href="{{ route('document.index') }}" class="nav-link
            @if(Route::current()->getName() == 'document.index')
              active
            @endif
            ">
              <i class="nav-icon fas fa-cog"></i>
              <p>{{__('general.list_docs')}}</p>
            </a>
          </li>
        @endcan
        @can('document_add')
          <li class="nav-item">
            <a href="{{ route('document.create') }}" class="nav-link
              @if(Route::current()->getName() == 'document.create')
                active
              @endif
            ">
              <i class="far fa-circle nav-icon"></i>
              <p>{{__('general.add_docs')}}</p>
            </a>
          </li>
        @endcan
      </ul>
    </li>
    @endcanany
    @can('role_show')
      <li class="nav-item">
        <a href="{{ route('report.form') }}"  class="nav-link">
          <i class="fas fa-circle nav-icon"></i>
          <p>{{__('general.reports')}}</p>
        </a>
      </li>
    @endcan
    @can('role_show')
      <li class="nav-item">
        <a href="{{ route('role.create') }}"  class="nav-link">
          <i class="fas fa-circle nav-icon"></i>
          <p>{{__('general.roles')}}</p>
        </a>
      </li>
    @endcan
    <hr/>
    <li class="nav-item">
      <form id="logout" action="{{ route('logout')}}" method="post">
        @csrf
      </form>
      <a href="javascript:{}" onclick="document.getElementById('logout').submit();" class="nav-link text-danger">
        <i class="fas fa-circle nav-icon"></i>
        <p>{{__('general.logout')}}</p>
      </a>
    </li>

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
