
@extends('app')

@section('page_title',__('general.add_payment'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.add_payment')}}</h3>
            </div>

            <div class="card-body">
              <form action="{{ route('payment.store') }}" method="POST">
                @csrf
                <div class="row">
                  <div class="col-sm-4">

                    <div class="radio">
                      <label>
                      <input type="radio" name="type"  value="apartment" >
                      {{__('general.is_apartment')}}
                      </label>
                      <label>
                      <input type="radio" name="type"  value="land" >
                      {{__('general.is_land')}}
                      </label>
                    </div>


                    <div class="form-group hidden apartment">
                      <label>{{__('general.apartment_name')}}<span class="text-danger"> * </span></label>

                      <select class="form-control select2" style="width: 100%;" name="apartment_id">
                          <option>{{__('general.select')}}</option>
                          @foreach ($apartments as $id => $apartment)
                            <option value="{{ $apartment->id }}">{{ $apartment->apartment_no.'-'.$apartment->resident->name.' '.$apartment->resident->father_name }}</option>
                          @endforeach
                      </select>
                    </div>

                    <div class="form-group hidden land">
                      <label>{{__('general.land_name')}}<span class="text-danger"> * </span></label>

                      <select class="form-control select2" style="width: 100%;" name="land_id">
                          <option>{{__('general.select')}}</option>
                          @foreach ($lands as $id => $land)
                            <option value="{{ $land->id }}">{{ $land->land_no.'-'.$land->resident->name.' '.$land->resident->father_name }}</option>
                          @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">{{__('general.bill_no')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('bill_no') }}" type="text" class="form-control"   name="awiz_no">
                    @error('awiz_no')
                        <span style="color: red;">* {{ $message }} </span>
                    @enderror
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">{{__('general.partial_no')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('partial_no') }}" type="number" class="form-control"   name="partial_no">
                    @error('partial_no')
                        <span style="color: red;">* {{ $message }} </span>
                    @enderror
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">{{__('general.amount')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('amount') }}" type="number" class="form-control"   name="amount">
                    @error('amount')
                        <span style="color: red;">* {{ $message }} </span>
                    @enderror
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">{{__('general.date')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('date') }}" type="text" class="form-control jalali-date"  name="date">
                    @error('date')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>

                  </div>

                  </div>
                  <button type="submit" class="btn btn-info btn-flat">{{__('general.add_payment')}}</button>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
@endsection

@pushOnce('datatables-script')
<script>
  $('input[type=radio][name=type]').change(function() {
      if (this.value == 'land') {

          $(".land").css("display", "block");
          $(".apartment").css("display", "none");
      }
      else if (this.value == 'apartment') {

          $(".apartment").css("display", "block");
          $(".land").css("display", "none");
      }
  });

</script>

@endpushOnce
