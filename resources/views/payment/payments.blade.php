@extends('app')  

@section('page_title','Payments')

@section('body') 
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Payments</h3>
            </div>
            <!-- /.card-header -->
            {{-- <h1>{{$name}}</h1> --}}
            <div class="card-body">
              <form action="" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <!-- radio -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary1" name="r1" checked="">
                            <label for="radioPrimary1">{{__('language.Land')}}</label>
                          </div>
                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary2" name="r1">
                            <label for="radioPrimary2">{{__('language.Apartment')}}</label>
                          </div>
                        </div>
                      </div>
                    <div class="form-group col-sm-4" data-select2-id="29">
                        <label>{{__('language.Apartment')}}</label>
                        <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="9" tabindex="-1" aria-hidden="true">
                        <option selected="selected" data-select2-id="11"></option>
                        <option data-select2-id="35">imran</option>
                        <option data-select2-id="40">Rohullah</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-4" data-select2-id="29">
                        <label>{{__('language.Resident')}}</label>
                        <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="9" tabindex="-1" aria-hidden="true">
                        <option selected="selected" data-select2-id="11"></option>
                        <option data-select2-id="35">imran</option>
                        <option data-select2-id="40">Rohullah</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                      <label for="exampleInputEmail1">{{__('language.AwizNo')}}</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter AwizNo">
                    </div>
                    <div class="form-group col-sm-4">
                      <label for="exampleInputEmail1">{{__('language.PartialNo')}}</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Partial No">
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="exampleInputEmail1">{{__('language.Amount')}}</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Amount">
                      </div>
                    <div class="form-group col-sm-4">
                        <label>{{__('language.Date')}}</label>
                          <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                          <input name="date" type="text" class="form-control datetimepicker-input example1" data-target="#reservationdatetime">
                              <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                          </div>
                    </div>
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">{{__('language.submit')}}</button>
                    </div>
                  </div>
              </form>
            </div>

          </div>
        </div>
@endsection