
@extends('app')

@section('page_title',__('general.get_report'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.get_report')}}</h3>
            </div>

            <div class="card-body">
              <form action="{{ route('report.generate') }}" method="POST">
                @csrf
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-md-6">
                      <table class="table">
                        <tr>
                          <td>
                            <label>{{__('general.property_type')}}</label>
                          </td>
                          <td>
                            <label>
                              <input type="radio" name="property_type"  value="apartment" >
                              {{__('general.is_apartment')}}
                            </label>
                          </td>
                          <td>
                            <label>
                              <input type="radio" name="property_type"  value="land" >
                              {{__('general.is_land')}}
                            </label>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <label>{{__('general.property_status')}}</label>
                          </td>
                          <td>
                            <label>
                              <input type="radio" name="property_status"  value="private" >
                              {{__('general.private')}}
                            </label>
                          </td>
                          <td>
                            <label>
                              <input type="radio" name="property_status"  value="government" >
                              {{__('general.government')}}
                            </label>
                          </td>
                        </tr>
                        <tr>
                          <td>
                              <label>{{__('general.payment_status')}}</label>
                          </td>
                          <td>
                            <label>
                            <input type="radio" name="payment_status"  value="paid" >
                            {{__('general.paid')}}
                            </label>
                          </td>
                          <td>
                            <label>
                            <input type="radio" name="payment_status"  value="due" >
                            {{__('general.due')}}
                            </label>
                          </td>
                        </tr>
                      </table>
                    </div>

                    <div class="col-md-6">

                      <div class="form-group">
                        <label>{{__('general.shahrak')}}</label>

                        <select class="form-control select2" style="width: 100%;" name="shahrak_id">
                          <option value="">{{ __('general.select_shahrak')}}</option>
                            @foreach ($shahraks as $shahrak)
                              <option value="{{ $shahrak->id }}">{{ $shahrak->shahrak_name }}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">{{__('general.start_date')}}</label>
                        <input value="{{ old('start_date') }}" type="text" class="form-control jalali-date"  name="start_date">
                        @error('date')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">{{__('general.end_date')}}</label>
                        <input value="{{ old('end_date') }}" type="tetx" class="form-control jalali-date"  name="end_date">
                        @error('date')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>
                    </div>

                  </div>

                  </div>
                  <button type="submit" class="btn btn-info btn-flat">{{__('general.get_report')}}</button>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
@endsection

@pushOnce('datatables-script')


@endpushOnce
