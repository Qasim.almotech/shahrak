
@extends('app')

@section('page_title',__('general.reports'))

@section('body')

@if ($message = session('message'))
<div class="callout callout-success">
  <p class="text-success">
    <b><i class="fas fa-info"></i></b> {{ $message }}
  </p>
</div>
@endif

<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <!-- /.card -->

          <div class="card">

            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                <thead>
                  <tr>
                    <th>{{ __('general.shahrak') }}</th>
                    <th>{{ __('general.owner') }}</th>
                    <th>{{ __('general.property_type') }} </th>
                    <th>{{ __('general.property_status') }}</th>
                    <th>{{ __('general.land_apt_no') }}</th>
                    <th>{{ __('general.area_size') }}</th>
                    <th>{{ __('general.total_price') }}</th>
                    <th>{{ __('general.persentage') }}</th>
                    <th>{{ __('general.net_amount') }}</th>
                    <th>{{ __('general.paid') }}</th>
                    <th>{{ __('general.due') }}</th>
                    <th>{{ __('general.date') }}</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data['land'] as $land)
                        <tr>
                            <td>
                                {{ $land->shahrak->shahrak_name  }}
                            </td>
                            <td>
                                {{ $land->resident->name.' '.$land->resident->father_name  }}
                            </td>

                            <td>
                                {{ __('general.is_land') }}
                            </td>

                            <td>
                                {{ __('general.'.$land->type) }}
                            </td>

                            <td>
                                {{ $land->land_no }}
                            </td>

                            <td>
                                {{ $land->area_size }}
                            </td>

                            <td>
                              {{ $land->total_price .' '.__('general.afn') }}
                            </td>
                            <td>
                                {{ $land->percentage }}
                            </td>
                            <td>
                                {{ $land->net_amount .' '.__('general.afn') }}
                            </td>
                            <td>
                                {{ $land->payments->sum('amount') .' '.__('general.afn')}}
                            </td>
                            <td>
                                {{ $land->net_amount - $land->payments->sum('amount') .' '.__('general.afn')}}
                            </td>
                            <td>
                                @php
                                  $date = explode('-', $land->date);
                                  echo gregorian_to_jalali($date[0], $date[1], $date[2]);
                                @endphp
                            </td>

                          </tr>
                    @endforeach
                    @foreach ($data['apartment'] as $apt)
                    <tr>
                        <td>
                            {{ $apt->shahrak->shahrak_name  }}
                        </td>
                        <td>
                            {{ $apt->resident->name.' '.$apt->resident->father_name  }}
                        </td>

                        <td>
                            {{ __('general.is_apartment') }}
                        </td>

                        <td>
                            {{ __('general.'.$apt->type) }}
                        </td>

                        <td>
                            {{ $apt->apartment_no }}
                        </td>

                        <td>
                            {{ $apt->area_size }}
                        </td>

                        <td>
                          {{ $apt->cost .' '.__('general.afn') }}
                        </td>
                        <td>
                            {{ $apt->percentage }}
                        </td>
                        <td>
                            {{ $apt->net_amount .' '.__('general.afn') }}
                        </td>
                        <td>
                            {{ $apt->payments->sum('amount') .' '.__('general.afn') }}
                        </td>
                        <td>
                            {{ $apt->net_amount - $apt->payments->sum('amount') .' '.__('general.afn') }}
                        </td>
                        <td>
                            @php
                              $date = explode('-', $apt->date);
                              echo gregorian_to_jalali($date[0], $date[1], $date[2]);
                            @endphp
                        </td>

                      </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>



@endsection

@pushOnce('datatables-script')
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>


<script>
  $(function () {
  $("#example1").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["csv", "excel"]
  }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });
});

</script>

@endpushOnce
