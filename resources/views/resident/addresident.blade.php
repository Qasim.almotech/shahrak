
@extends('app')

@section('page_title',__('general.add_resident'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.add_resident')}}</h3>
            </div>

            <div class="card-body">
              <form action="{{ route('resident.store') }}" method="POST">
                @csrf
                <div class="row">
                  <div class="form-group col-sm-6">
                      <label for="exampleInputEmail1">{{__('general.name')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('name') }}" type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                      @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                  </div>
                    <div class="form-group col-sm-6">
                      <label for="exampleInputEmail1">{{__('general.father_name')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('f_name') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Father Name" name="f_name">
                      @error('f_name')
                        <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group col-sm-12">
                      <label for="exampleInputEmail1">{{__('general.grandfather_name')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('gf_name') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Garand Father Name" name="gf_name">
                      @error('gf_name')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group col-sm-4">
                      <label for="exampleInputEmail1">{{__('language.NIC')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('tazkira') }}" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter NIC" name="tazkira">
                      @error('tazkira')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group col-sm-4">
                      <label for="exampleInputEmail1">{{__('language.Book')}}</label>
                      <input value="{{ old('book') }}" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Book" name="book">
                     @error('book')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group col-sm-4">
                      <label for="exampleInputEmail1">{{__('language.Page')}}</label>
                      <input value="{{ old('f_name') }}" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Page" name="page">
                     @error('page')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label for="exampleInputEmail1">{{__('general.main_address')}}</label>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">{{__('language.province')}}<span class="text-danger"> * </span></label>
                        <input value="{{ old('province') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter province" name="province">
                        @error('province')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>


                      <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">{{__('language.District')}}<span class="text-danger"> * </span></label>
                        <input value="{{ old('district') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter district" name="district">
                        @error('district')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>


                      <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">{{__('language.Village')}}<span class="text-danger"> * </span></label>
                        <input value="{{ old('village') }}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Village" name="village">
                        @error('village')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                      </div>
                    </div>

                  </div>
                  <button type="submit" class="btn btn-info btn-flat">{{__('general.add_resident')}}</button>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
@endsection
