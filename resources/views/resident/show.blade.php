@extends('app')

@section('page_title',__('general.resident_show'))

@section('body')
@if ($message = session('message'))
<div class="callout callout-success">
  <p class="text-success">
    <b><i class="fas fa-info"></i></b> {{ $message }}
  </p>
</div>
@endif


<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <!-- /.card -->

        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <table class="table table-sm table-bordered" >
                  <h5>{{__('general.resident_info')}}</h5>
                  <tbody>
                    <tr>
                      <th>
                      {{__('general.name')}}
                      </th>
                      <td>
                        {{ $resident->name}}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('general.father_name')}}
                      </th>
                      <td>
                        {{ $resident->father_name}}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('general.grandfather_name') }}
                      </th>
                      <td>
                        {{ $resident->grandfather_name }}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('general.NIC')}}
                      </th>
                      <td>
                        {{ $resident->NIC }}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('language.Book')}}
                      </th>
                      <td>
                        {{ $resident->book }}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('language.Page')}}
                      </th>
                      <td>
                        {{ $resident->page }}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('general.province')}}
                      </th>
                      <td>
                        {{ $resident->province }}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('general.district')}}
                      </th>
                      <td>
                        {{ $resident->district}}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        {{__('general.village')}}
                      </th>
                      <td>
                        {{ $resident->village}}
                      </td>
                    </tr>

                  </tbody>
                </table>
              </div>
              <div class="col-md-6">
                <h5>{{__('general.resident_property')}}</h5>
                <table class="table table-sm table-bordered" >
                  <thead>
                    <tr>
                      <th>
                        {{__('general.type')}}
                      </th>
                      <th>
                        {{__('general.land_apt_no')}}
                      </th>
                      <th>
                        {{__('general.persentage')}}
                      </th>
                      <th>
                        {{__('general.net_amount')}}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($resident->lands as $land)
                      <tr>
                        <td>
                          {{__('general.is_land')}}
                        </td>
                        <td>
                          {{ $land->land_no}}
                        </td>
                        <td>
                          {{ $land->percentage }}
                        </td>
                        <td>
                          {{ $land->net_amount.' '.__('general.afn') }}
                        </td>
                      </tr>
                    @endforeach
                    @foreach($resident->apartments as $apt)
                      <tr>
                        <td>
                          {{__('general.is_apartment')}}
                        </td>
                        <td>
                          {{ $apt->apartment_no}}
                        </td>
                        <td>
                          {{ $apt->percentage }}
                        </td>
                        <td>
                          {{ $apt->net_amount.' '.__('general.afn') }}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>

@endsection
