
@extends('app')

@section('page_title',__('general.add_shahrak'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.add_shahrak')}}</h3>
            </div>

            <div class="card-body">
              <form action="{{ route('shahrak.store') }}" method="POST">
                  @csrf
                  <div class="form-row">
                    <div class="col-md-12">
                      <h4>{{__('general.shahrak_information')}}</h4>
                       <hr/>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="Shahrak Name">{{__('general.shahrak_name')}}<span class="text-danger"> * </span></label>
                      <input id="shahrak_name" type="text" class="form-control input-sm rounded-0" id="Shahrak name" name="shahrakname" placeholder="{{__('general.shahrak_name')}}" value="{{ old('shahrakname') }}">

                      @error('shahrakname')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror

                    </div>

                    <div class="form-group col-md-4">
                      <label for="inputPassword4">{{__('general.district')}}<span class="text-danger"> * </span></label>
                    @error('District')
                      <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                      <div class="input-group-append">
                           <select name="District" class="form-control" style="width:100%;">
                             <option value="">{{__('general.select_district')}}</option>

                              @for($x = 1; $x <= 22; $x++)
                                <option value="ds{{sprintf('%02d', $x)}}">{{__('district.ds'.sprintf("%02d", $x))}}</option>
                              @endfor
                            </select>
                      </div>
                  </div>

                  </div>
                  <div class="form-row">

                  <div class="form-group col-md-4">
                      <label for="inputState">{{__('general.longtitude') }}<span class="text-danger"> * </span></label>

                      <input name="Longtitude" value="{{ old('Longtitude') }}" type="text" class="form-control input-sm rounded-0" id="inputZip" placeholder="34.5119408,69">
                    @error('Longtitude')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputZip">{{__('general.lititude') }}<span class="text-danger"> * </span></label>

                      <input name="Lititude" value="{{ old('Lititude') }}" type="text" class="form-control input-sm rounded-0" id="phonenumber" placeholder="1804592,15z">
                    @error('Lititude')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror

                  </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="inputCity">{{__('general.owner_name')}}<span class="text-danger"> * </span></label>
                      <input name="Name" value="{{ old('Name') }}"  type="text" class="form-control input-sm rounded-0" id="inputCity" >
                    @error('Name')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputState">{{__('general.father_name')}}<span class="text-danger"> * </span></label>

                      <input name="Father_Name" value="{{ old('Father_Name') }}" type="text" class="form-control input-sm rounded-0" id="inputZip" >
                    @error('Father_Name')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputState">{{__('general.grandfather_name')}}<span class="text-danger"> * </span></label>

                      <input name="GrandFather_Name" value="{{ old('GrandFather_Name') }}" type="text" class="form-control input-sm rounded-0 rounded-0" id="inputZip" >
                    @error('GrandFather_Name')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror

                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputState">{{__('general.NIC')}}<span class="text-danger"> * </span></label>

                      <input name="Tazkira_Number" value="{{ old('Tazkira_Number') }}"  type="number" class="form-control input-sm rounded-0 rounded-0 " id="inputZip" placeholder="13840912984093">
                      @error('Tazkira_Number')
                        <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputZip">{{__('general.contact_number')}}<span class="text-danger"> * </span></label>
                    <div class="input-group-prepend">
                          <span class="input-group-text rounded-0"><i class="fas fa-phone"></i> &nbsp;&nbsp;&nbsp;&nbsp; +93</span>
                      <input name="contact_number" value="{{ old('Contactnumber') }}"  type="number" class="form-control input-sm rounded-0" id="phonenumber" placeholder="781 567 567"><br>

                    </div>
                    @error('contact_number')
                      <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                  </div>
                  <br>
                  <div class="row">
                      <div class="col-md-12">
                        <h4>{{__('general.contract_information')}}</h4>
                        <hr/>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.contract_company')}}</label>
                          <input class="form-control" name="contract_company" value="{{ old('contract_company') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.contract_no')}}</label>
                          <input class="form-control" name="contract_no" value="{{ old('contract_no') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.contract_date')}}</label>
                          <input class="form-control jalali-date" name="contract_date" value="{{ old('contract_date') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.sentence_no')}}</label>
                          <input class="form-control" name="sentence_no" value="{{ old('sentence_no') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.contract_department')}}</label>
                          <input class="form-control" name="contract_department" value="{{ old('contract_department') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.monitoring')}}</label>
                          <input class="form-control" name="monitoring" value="{{ old('monitoring') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.contract_percentage')}}</label>
                          <input class="form-control" name="contract_percentage" value="{{ old('contract_percentage') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.duration_date')}}</label>
                            <input class="form-control" name="duration_date" value="{{ old('duration_date') }}">
                          </div>
                        </div>
                      </div>
                  </div>
                  <br/>
                  <div class="row">
                      <div class="col-md-12">
                        <h4>{{__('general.plan_information')}}</h4>
                        <hr>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.mutch_land')}}</label>
                          <input class="form-control" name="land_numbers" value="{{ old('land_numbers') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.mutch_blocks')}}</label>
                          <input class="form-control" name="mutch_blocks" value="{{ old('mutch_blocks') }}">
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputCity">{{__('general.total_area_size')}} <span class="text-danger"> * </span></label>
                        <input type="number" class="form-control input-sm rounded-0" id="inputCity" placeholder="{{__('general.m2')}}" name="Totalsize" value="{{ old('Totalsize') }}">
                        @error('Totalsize')
                          <span style="color: red;">*  {{ $message }} </span>
                        @enderror

                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <div class="form-group col-md-12">
                            <label >{{__('general.other_area_size')}}</label>
                          <input class="form-control" name="other_area_size" value="{{ old('other_area_size') }}">
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="form-group col-md-12">
                      <label for="inputZip">{{__('general.description')}}</label>
                    <textarea class="form-control" name="Details"  id="" rows="3" style="resize:none; width:100%;" placeholder="جزئیات اضافی را در مورد شهرک در اینجا تایپ کنید">{{ old('Details') }}</textarea>
                    </div>
                  </div>

                  <button type="submit" id="save" onclick="confirmsave()" class="btn btn-info btn-flat">{{__('general.add_shahrak')}}</button>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>



@endsection
