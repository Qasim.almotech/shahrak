<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('/dist/img/km-icon.png') }}">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>{{__('general.show_shahrak')}}</title>
    <style>
      .rtl{
        direction: rtl;
      }
      @font-face {
        font-family: 'Nassim';
        src: url('{{ asset('/dist/fonts/BBCNassim.eot?#') }}') format('eot'),
             url('{{ asset('/dist/fonts/Nassim.woff') }}') format('woff'),
             url('{{ asset('/dist/fonts/BBCNassim.ttf') }}') format('truetype');
      }
      @font-face {
        font-family: 'Nassim-normal';
        src: url('{{ asset('/dist/fonts/BBCNassim_3.eot?#') }}') format('eot'),
             url('{{ asset('/dist/fonts/BBCNassim_3.woff') }}') format('woff'),
             url('{{ asset('/dist/fonts/BBCNassim_3.ttf') }}') format('truetype');
      }
      body{
      	font-family: Nassim-normal;
      }
    </style>
  </head>
  <body class="rtl">
    <div class="container" style="padding-top:50px;">
      <div class="row">
        <div class="col-md-3">
          <img src="{{ asset('/dist/img/km-icon.png') }}" width="250px">
        </div>
        <div class="col-md-6">
          <h4 style="text-align: center">
                امارت اسلامی افغانستان
            <br>شاروالی کابل
            <br>معینیت پلان گذاری و توسعه شهری
            <br>ریاست ملکیت ها
            <br>آمریت تحصیل عواید املاکی
            <br>مدیریت شهرک ها
          </h4>
        </div>

        <div class="col-md-3">
          <img src="{{ asset('/dist/img/emarat-logo.png') }}" width="170px" style="float:left;">
        </div>
      </div>
      <br/>
      <div class="row">
        <div class="col-md-6">

          <table class="table table-bordered">

            <tr>
              <th>
              {{__('general.shahrak_name')}}
              </th>
              <td>
                {{ $shahrak->shahrak_name}}
              </td>
              <td>
              </td>
            </tr>

            <tr>
              <th>
                {{__('general.contract_company')}}
              </th>
              <td>
                {{ $shahrak->contract_company}}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>
                {{__('general.district')}}
              </th>
              <td>
                {{ __('district.'.$shahrak->district) }}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>
                {{__('general.contract_no')}}
              </th>
              <td>
                {{ $shahrak->contract_no}}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>{{__('general.sentence_no') }}</th>
              <td>
                {{ $shahrak->sentence_no }}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>
                {{__('general.contract_department')}}
              </th>
              <td>
                {{ $shahrak->contract_department}}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>
                {{ __('general.monitoring')}}
              </th>
              <td>
                {{ $shahrak->monitoring }}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>{{__('general.total_area_size')}}</th>
              <td>
                {{ $shahrak->total_area_size}}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>{{__('general.contract_percentage')}}</th>
              <td>
                {{ $shahrak->contract_percentage}}
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <th>{{__('general.duration_date')}}</th>
              <td>
                {{$shahrak->duration_date}}
              </td>
              <td>

              </td>
            </tr>
          </table>
        </div>
        <div class="col-md-6">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4021.7260937849996!2d69.17832227920205!3d34.56418224934163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38d16e62a9fc841f%3A0xc89971d97258dfc0!2sAria%20Town%2C%20Kabul%2C%20Afghanistan!5e0!3m2!1sen!2s!4v1665039688221!5m2!1sen!2s"
                  width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"
                  referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>

      </div>
      <div class="row">
      <div class="col-md-12">
        <caption>ارایه معلومات در رابطه به تعداد توزیع نمرات و تحصیل سهم عواید شاروالی کابل</caption>
        <table class="table table-bordered">
          <tr>
            <th>{{__('general.description_plan')}}</th>
            <th>{{__('general.numbers_by_plan')}}</th>
            <th>{{__('general.published_apt_land')}}</th>
            <th>{{__('general.balance')}}</th>
            <th>{{__('general.paid_afg')}}</th>
            <th>{{__('general.comment')}}</th>
          </tr>
          <tr>
            <th>
              {{__('general.mutch_land') }}
            </th>
            <td>
              {{ $shahrak->no_land}}
            </td>
            <td>
              {{ $shahrak->lands->count()}}
            </td>
            <td>
              {{ $shahrak->no_land - $shahrak->lands->count() }}
            </td>
            <td>
              {{ $paid['land_paid'] .' '.__('general.afn') }}
            </td>
            <td></td>
          </tr>
          <tr>

            <th>
              {{__('general.mutch_blocks') }}
            </th>
            <td>
              {{ $shahrak->mutch_blocks }}
            </td>
            <td>
              {{ $shahrak->apartments->count() }}
            </td>
            <td>
              {{ $shahrak->mutch_blocks - $shahrak->apartments->count() }}
            </td>
            <td>
              {{ $paid['apt_paid'] .' '.__('general.afn') }}
            </td>
            <td></td>
          </tr>
          <tr>
            <th>
              {{__('general.other_area_size') }}
            </th>
            <td>
              {{ $shahrak->other_area_size }}
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <th>{{__('general.total')}}</th>
            <td>
                {{ $shahrak->no_land + $shahrak->mutch_blocks}}
            </td>
            <td>
              {{ $shahrak->lands->count() + $shahrak->apartments->count()}}
            </td>
            <td>
              {{ ($shahrak->mutch_blocks - $shahrak->apartments->count()) + ($shahrak->no_land - $shahrak->lands->count())}}
            </td>
            <td>
              {{ ($paid['apt_paid'] + $paid['land_paid']) .' '.__('general.afn') }}
            </td>
            <td></td>
          </tr>
          <tr>
            <th></th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <th>{{__('general.pendding_payment')}}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
      </table>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <br/>
        <footer class="main-footer">
          <center><strong>{{__('general.footer_copyright')}}</strong></center>
        </footer>
        <br/>
      </div>
    </div>
  </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>
