
@extends('app')

@section('page_title', __('general.404'))

@section('body')
@if ($message = session('message'))
<div class="callout callout-success">
  <p class="text-success">
    <b><i class="fas fa-info"></i></b> {{ $message }}
  </p>
</div>
@endif

<section class="content">
  <div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>
    <div class="error-content">
      <h3><i class="fa fa-warning text-yellow"></i> {{__('general.page_not_found')}}.</h3>
      <p>
        {{__('general.you_do_not_have_permission')}}.<br/>
        {{__('general.return')}} <a href="{{ route('dashboard') }} "> {{__('general.to_dashboard')}}</a>.
      </p>

    </div><!-- /.error-content -->
  </div><!-- /.error-page -->
</section>

@endsection
