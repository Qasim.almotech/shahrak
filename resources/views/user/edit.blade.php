@extends('app')

@section('page_title',__('general.edit_user'))

@section('body')
<div class="card card-secondary">
    <div class="card-header">
      <h3 class="card-title rtl">{{__('general.edit_user')}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form method="POST" action="{{ route('user.update' , $user) }}">
          @csrf
          @method('PUT')
          <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('general.name') }}</label>

              <div class="col-md-6">
                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required  autofocus>

                  @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="row mb-3">
              <label for="last_name" class="col-md-4 col-form-label text-md-end">{{ __('general.last_name') }}</label>

              <div class="col-md-6">
                  <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ $user->last_name }}" required autocomplete="last_name" autofocus>

                  @error('last_name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="row mb-3">
              <label for="position" class="col-md-4 col-form-label text-md-end">{{ __('general.position') }}</label>

              <div class="col-md-6">
                  <input id="position" type="text" class="form-control @error('position') is-invalid @enderror" name="position" value="{{ $user->position }}" required autocomplete="position" autofocus>

                  @error('position')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="row mb-3">
              <label for="contact_number" class="col-md-4 col-form-label text-md-end">{{ __('general.contact_number') }}</label>

              <div class="col-md-6">
                  <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{ $user->contact_number }}" required autocomplete="contact_number" autofocus>

                  @error('contact_number')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="row mb-3">
              <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('general.email') }}</label>

              <div class="col-md-6">
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required >

                  @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="row mb-3">
              <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('general.password') }}</label>

              <div class="col-md-6">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" >

                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="row mb-3">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('general.confirm_password') }}</label>

              <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
              </div>
          </div>
          <div class="row mb-3">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('general.roles') }}</label>
              <div class="col-md-6">
                <select class="form-control" style="width: 100%;" name="role">
                  @foreach($roles as $role)
                      <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
              </div>
          </div>

          <div class="row mb-0">
              <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                      {{ __('general.edit_user') }}
                  </button>
              </div>
          </div>
      </form>
    </div>
    <!-- /.card-body -->
</div>


</div>


@endsection
