
@extends('app')

@section('page_title', __('general.user_list'))

@section('body')
@if ($message = session('message'))
<div class="callout callout-success">
  <p class="text-success">
    <b><i class="fas fa-info"></i></b> {{ $message }}
  </p>
</div>
@endif


<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <!-- /.card -->

          <div class="card">


            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                <thead>
                <tr>
                  <th>{{ __('general.name') }} </th>
                  <th>{{ __('general.email') }}</th>
                  <th>{{ __('general.position') }}</th>
                  <th>{{ __('general.roles') }}</th>
                  <th>{{ __('general.contact_number') }}</th>
                  <th>{{ __('general.options') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $item)
                        <tr>
                            <td>
                                {{ $item->name.' '.$item->last_name }}
                            </td>
                            <td>
                                {{ $item->email }}
                            </td>
                            <td>
                                {{ $item->position }}
                            </td>
                            <td>
                                @foreach($item->getRoleNames() as $role)
                                  {{ $role }}
                                @endforeach
                            </td>
                            <td>
                              {{ $item->contact_number }}
                            </td>
                            <td>
                              <a class="btn btn-info btn-xs" href="{{ route('user.edit', $item->id) }}">{{__('general.edit')}}</a>

                              <a class="btn btn-danger btn-xs" onclick="confirmDelete({{ $item->id }})" href="#">{{__('general.delete')}}</a>

                              <form id="delete{{$item->id}}" action="{{ route('user.destroy', $item->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                              </form>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>


@endsection


@pushOnce('datatables-script')
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>


<script>
  $(function () {
  $("#example1").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });
});

</script>

<script>
function confirmDelete(id) {
  let text = 'آیا مطمین استید تا این ریکارد حذف شود؟';
  if (confirm(text) == true) {
    $('form#delete'+id).submit();

  } else {
    alert('ریکارد حذف نشد.');
  }

}
</script>

@endpushOnce
