<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShahrakController;

use App\Http\Controllers\ResidentController;
use App\Http\Controllers\LandController;

use App\Http\Controllers\ApartmentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ReportController;
use Faker\Provider\ar_EG\Payment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::get('/', [HomeController::class, 'index'])->name('dashboard');
Route::get('/countapartment', [HomeController::class, 'countapartment']);
Route::get('/countland', [HomeController::class, 'countland']);
Route::get('/privateandgovernment', [HomeController::class, 'privateandgovernment']);
Route::resource('resident', ResidentController::class);
Route::resource('land', LandController::class);
Route::resource('apartment', ApartmentController::class);
Route::resource('payment', PaymentController::class);
Route::resource('role', RoleController::class);
Route::resource('user', UserController::class);
Route::resource('document', DocumentController::class);
Route::get('report', [ReportController::class, 'form'])->name('report.form');
Route::post('report', [ReportController::class, 'generate'])->name('report.generate');
Route::resource('shahrak', ShahrakController::class);
